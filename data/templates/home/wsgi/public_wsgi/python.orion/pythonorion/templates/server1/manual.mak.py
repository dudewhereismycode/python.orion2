# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1569966118.11667
_enable_loop = True
_template_filename = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/manual.mak'
_template_uri = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/manual.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('<!DOCTYPE html>\n<html>\n<title>GPScontrol VideoManuales</title>\n<meta name="viewport" content="width=device-width, initial-scale=1">\n<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">\n<body>\n    <!-- Sidebar -->\n    <div class="w3-sidebar w3-light-grey w3-bar-block" style="width:15%">\n      <p class="w3-bar-item">Plataforma</p>\n      <a href="#ticket" class="w3-bar-item w3-button">Crear Ticket</a>\n      <a href="#vehiculo" class="w3-bar-item w3-button">Información del Vehículo</a>\n      <a href="#contactos" class="w3-bar-item w3-button">Contactos de Emergencia</a>\n      <p class="w3-bar-item">Servicios</p>\n      <a href="#chekdo" class="w3-bar-item w3-button">CHEKDO</a>\n    </div>\n\n    <!-- Page Content -->\n    <div style="margin-left:15%">\n\n        <div class="w3-container" style="background-color:#27323a;color:#FFFFFF" id="ticket">\n          <h1>Nuevo Ticket</h1>\n        </div>\n\n        <video style="width:100%" controls>\n          <source src="')
        __M_writer(escape(h.url()))
        __M_writer('/img/video/crearticket.mp4" type="video/mp4">\n          Your browser does not support HTML5 video.\n        </video>\n\n        <div class="w3-container">\n        <h2>Nuevo Ticket</h2>\n        <p>Levanta un ticket de Soporte Técnico sin salir de tu plataforma. En menos de 48hr nuestros especialistas te estarán contactando para darte una respuesta y solución a tu ticket</p>\n        </div>\n\n        <div class="w3-container" style="background-color:#27323a;color:#FFFFFF" id="vehiculo">\n          <h1>Información del Vehículo</h1>\n        </div>\n\n        <video style="width:100%" controls>\n          <source src="')
        __M_writer(escape(h.url()))
        __M_writer('/img/video/informacionVehicular.mp4" type="video/mp4">\n          Your browser does not support HTML5 video.\n        </video>\n\n        <div class="w3-container">\n        <h2>Actualiza tu Información</h2>\n        <p>Te pedimos tener actualizada la información de tus vehículos para que al momento de activarse un\n            Protocolo de Emergencia nuestro Centro de Monitore tenga la información completa y se pueda tener una reacción inmediata</p>\n        </div>\n\n        <div class="w3-container" style="background-color:#27323a;color:#FFFFFF" id="contactos">\n          <h1>Contactos de Emergencia</h1>\n        </div>\n\n        <video style="width:100%" controls>\n          <source src="')
        __M_writer(escape(h.url()))
        __M_writer('/img/video/contactosdeemergencia.mp4" type="video/mp4">\n          Your browser does not support HTML5 video.\n        </video>\n\n        <div class="w3-container">\n        <h2>Estamos en Contacto</h2>\n        <p>Para nosotros es importante contar con los contactos actualizados del pesonal que puede recibir notificaciónes de tus unidades, ya sea para promociones, seguimientos de tickets,\n            emergencias, alertas, o saber tu personal de confianza. No olvides dar de baja a las personas que ya no se encuentren laborando contigo mediante un correo a soporte@gpscontrol.com</p>\n        </div>\n\n        <div class="w3-container" style="background-color:#27323a;color:#FFFFFF" id="chekdo">\n            <h1>CHEKDO</h1>\n        </div>\n\n        <video style="width:100%" controls>\n            <source src="')
        __M_writer(escape(h.url()))
        __M_writer('/img/video/Checkdo_Plataforma.mp4" type="video/mp4">\n            Your browser does not support HTML5 video.\n        </video>\n\n        <div class="w3-container">\n            <h2>CHEKDO</h2>\n            <p>Ideal para la supervisión del cumplimiento de los trabajos en campo como: toma de pedidos, puntos de trabajo, distribución, entregas, etc.</p>\n        </div>\n    </div>\n</body>\n</html>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "uri": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/manual.mak", "filename": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/manual.mak", "line_map": {"17": 0, "37": 31, "23": 1, "24": 25, "25": 25, "26": 39, "27": 39, "28": 54, "29": 54, "30": 69, "31": 69}}
__M_END_METADATA
"""
