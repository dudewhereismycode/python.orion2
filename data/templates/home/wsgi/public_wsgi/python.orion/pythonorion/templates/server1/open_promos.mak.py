# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1569965723.605169
_enable_loop = True
_template_filename = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/open_promos.mak'
_template_uri = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/open_promos.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        video = context.get('video', UNDEFINED)
        kw = context.get('kw', UNDEFINED)
        h = context.get('h', UNDEFINED)
        image = context.get('image', UNDEFINED)
        __M_writer = context.writer()
        if image:
            if video:
                __M_writer('        <video width="50%" autoplay controls>\n          <source src="data:video/mp4;base64,')
                __M_writer(escape(kw['img']))
                __M_writer('" type="video/mp4">\n          <source src="data:video/mov;base64,')
                __M_writer(escape(kw['img']))
                __M_writer('" type="video/mov">\n          Tu navegador no soporta videos HTML5.\n        </video>\n')
            else:
                __M_writer('       <a title="PMO" ')
                __M_writer(escape(kw['url']))
                __M_writer(' target="_blank"><img style="width:50%;height:50%;" src="data:image/jpeg;base64,')
                __M_writer(escape(kw['img']))
                __M_writer('"/></a>\n')
        else:
            __M_writer('    <img style="width:50%;height:50%;alignment: center;" src="')
            __M_writer(escape(h.url()))
            __M_writer('/img/promodefault.png" alt="Promotion">\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/open_promos.mak", "filename": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/open_promos.mak", "line_map": {"32": 5, "33": 8, "34": 9, "35": 9, "36": 9, "37": 9, "38": 9, "39": 11, "40": 12, "41": 12, "42": 12, "48": 42, "17": 0, "26": 1, "27": 2, "28": 3, "29": 4, "30": 4, "31": 5}, "source_encoding": "utf-8"}
__M_END_METADATA
"""
