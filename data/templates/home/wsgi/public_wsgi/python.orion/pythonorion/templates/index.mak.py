# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1569975102.08236
_enable_loop = True
_template_filename = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/index.mak'
_template_uri = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/index.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = ['title']


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'local:templates.master', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n')
        __M_writer('\n<body background="')
        __M_writer(escape(h.url()))
        __M_writer('/img/orion.jpg">\n\n\n<h1><center>Orion</center></h1>\n<br>\n  <br>\n\n  <button id="enterButton" ></button>\n\n\n\n\n</body>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


def render_title(context):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_writer = context.writer()
        __M_writer('\n  Orion\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"filename": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/index.mak", "line_map": {"34": 1, "35": 5, "36": 6, "37": 6, "43": 3, "28": 0, "53": 47, "47": 3}, "source_encoding": "utf-8", "uri": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/index.mak"}
__M_END_METADATA
"""
