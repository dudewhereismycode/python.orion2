# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1569965727.6797621
_enable_loop = True
_template_filename = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/upload_promos.mak'
_template_uri = '/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/upload_promos.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        kw = context.get('kw', UNDEFINED)
        _ = context.get('_', UNDEFINED)
        h = context.get('h', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('<script>\n    function savePromoFileOrion(){\n        var input = document.querySelector(\'input[type=file]\');\n        file = input.files[0];\n        var expiration = $(\'#expiration_promo_file_orion\').val();\n        var url = $(\'#expiration_url_orion\').val();\n        var formData = new FormData();\n        formData.append("image", file);\n        formData.append("expiration", expiration);\n        formData.append("url", url);\n        formData.append("user", "')
        __M_writer(escape(kw['user']))
        __M_writer('");\n        formData.append("application_id", "')
        __M_writer(escape(kw['application_id']))
        __M_writer('");\n        formData.append("internal_id", "')
        __M_writer(escape(kw['internal_id']))
        __M_writer('");\n        var request = new XMLHttpRequest();\n        request.open("POST", \'')
        __M_writer(escape(h.url()))
        __M_writer('/promos/savepromo\');\n        request.send(formData);\n        request.onload  = function() {\n            var response = JSON.parse(request.responseText);\n            if (response.error == "ok"){\n                $.alert("')
        __M_writer(escape(_('Promo File Saved')))
        __M_writer('",{type: "success"});\n                closeSunWindow();\n            }else{\n                $.alert(response.error,{type: "warning"});\n            }\n        }\n    }\n</script>\n<form id="addPromoFileOrion">\n    <fieldset>\n        <table style="width:100%">\n            <tr>\n                <td style="width:30%">\n                    ')
        __M_writer(escape(_('Promo Expiration')))
        __M_writer(':\n                </td>\n                <td>\n                    <input type="date" id="expiration_promo_file_orion" style="width: 100%">\n                </td>\n            </tr>\n            <tr style="height: 10px">\n                <td></td>\n            </tr>\n            <tr>\n                <td style="width:30%">\n                    ')
        __M_writer(escape(_('Url')))
        __M_writer(':\n                </td>\n                <td>\n                    <input type="text" id="expiration_url_orion" style="width: 100%">\n                </td>\n            </tr>\n            <tr style="height: 10px">\n                <td></td>\n            </tr>\n            <tr>\n                <td style="width:30%">\n                    ')
        __M_writer(escape(_('Add File')))
        __M_writer(':\n                </td>\n                <td >\n                    <input type="file" id="promo_file_orion">\n                </td>\n            </tr>\n            <tr style="height: 100px">\n                <td></td>\n            </tr>\n        </table>\n    </fieldset>\n</form>\n<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="savePromoFileOrion()" style="padding:5px;float: right"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;')
        __M_writer(escape(_('Save')))
        __M_writer('</button>\n\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"uri": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/upload_promos.mak", "filename": "/home/wsgi/public_wsgi/python.orion/pythonorion/templates/server1/upload_promos.mak", "line_map": {"32": 15, "33": 15, "34": 20, "35": 20, "36": 33, "37": 33, "38": 44, "39": 44, "40": 55, "41": 55, "42": 67, "43": 67, "49": 43, "17": 0, "25": 1, "26": 11, "27": 11, "28": 12, "29": 12, "30": 13, "31": 13}, "source_encoding": "utf-8"}
__M_END_METADATA
"""
