# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1529072042.56655
_enable_loop = True
_template_filename = '/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altaresultadopartido.mak'
_template_uri = '/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altaresultadopartido.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'local:templates.master', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        h = context.get('h', UNDEFINED)
        allrecords = context.get('allrecords', UNDEFINED)
        cantidad = context.get('cantidad', UNDEFINED)
        _ = context.get('_', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n')
        __M_writer('\n\n<script>\n\n\n    function changestatepartido(id) {\n        var tochange=id.id;\n        $.ajax({\n            type: "GET",\n            url: "')
        __M_writer(escape(h.url()))
        __M_writer('/alta/habilita?id=" + tochange,\n            contentType: "application/json; charset=utf-8",\n            data: {},\n            success: function (parameterdata) {\n                //Insert HTML code\n              document.getElementById("status"+parameterdata.id).innerText=parameterdata.soy;\n\n            },\n            error: function () {\n\n            },\n            complete: function () {\n            }\n        });\n\n\n      }\n\n\n\n    function ingresaresultado() {\n        var arregloresultados = [];\n        var equipos = [];\n        for(var i=0;i<')
        __M_writer(escape(cantidad))
        __M_writer(';i++){\n                var resultadolocal=document.getElementById("L"+i).value;\n                var teamlocal=document.getElementById("teamL"+i).innerText;\n\n                var resultadovisita=document.getElementById("V"+i).value;\n                var teamvisita=document.getElementById("teamV"+i).innerText;\n\n\n                arregloresultados.push(resultadolocal);\n                arregloresultados.push(resultadovisita);\n                equipos.push(teamlocal);\n                equipos.push(teamvisita);\n        }\n\n        var confirma=confirm("¿Estas seguro de guardar el resultado?");\n        if(confirma){\n            $.ajax({\n                    type: "GET",\n                    url: "')
        __M_writer(escape(h.url()))
        __M_writer('/alta/elresultadopartido?equipos=" + equipos + "&resultados=" + arregloresultados,\n                    contentType: "application/json; charset=utf-8",\n                    data: {},\n                    success: function (parameterdata) {\n                        //Insert HTML code\n                        alert("EXITOSO");\n\n                    },\n                    error: function () {\n                        alert("ERROR");\n                    },\n                    complete: function () {\n                    }\n            });\n        }\n      }\n\n</script>\n\n')

        i=0
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['i'] if __M_key in __M_locals_builtin_stored]))
        __M_writer('\n\n\n')
        for item in allrecords:
            __M_writer('    <p> <input type="text" id=\'L')
            __M_writer(escape(i))
            __M_writer('\' value="')
            __M_writer(escape(item.puntoslocal))
            __M_writer('" size=10> <a id="teamL')
            __M_writer(escape(i))
            __M_writer('">')
            __M_writer(escape(item.nombrelocal))
            __M_writer(' </a>\n        ')
            __M_writer(escape(_('vs')))
            __M_writer('  <a id="teamV')
            __M_writer(escape(i))
            __M_writer('">')
            __M_writer(escape(item.nombrevisita))
            __M_writer(' </a><input type="text" id=\'V')
            __M_writer(escape(i))
            __M_writer('\'  value="')
            __M_writer(escape(item.puntosvisita))
            __M_writer('" size=10>\n        <button onclick="changestatepartido(this);" id="up-')
            __M_writer(escape(item.idpartido))
            __M_writer('">')
            __M_writer(escape(_('UP')))
            __M_writer('</button> <button onclick="changestatepartido(this);" id="down-')
            __M_writer(escape(item.idpartido))
            __M_writer('">')
            __M_writer(escape(_('DOWN')))
            __M_writer('\n        </button>\n         <div id="status')
            __M_writer(escape(item.idpartido))
            __M_writer('">\n')
            if item.habilitado == 0:
                __M_writer('             ')
                __M_writer(escape(_('DOWN')))
                __M_writer('\n')
            elif item.habilitado == 1:
                __M_writer('             ')
                __M_writer(escape(_('UP')))
                __M_writer('\n')
            else:
                __M_writer('             ')
                __M_writer(escape(_('NULL')))
                __M_writer('\n')
            __M_writer('        </div>\n\n    </p>\n\n    ')

            i=i+1
            
            
            __M_locals_builtin_stored = __M_locals_builtin()
            __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['i'] if __M_key in __M_locals_builtin_stored]))
            __M_writer('\n\n')
        __M_writer('\n <button type="button"  onclick="ingresaresultado();" class="btn btn-primary">')
        __M_writer(escape(_('SAVE RESULT')))
        __M_writer('</button>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "filename": "/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altaresultadopartido.mak", "line_map": {"28": 0, "37": 1, "38": 2, "39": 11, "40": 11, "41": 34, "42": 34, "43": 52, "44": 52, "45": 71, "51": 73, "52": 76, "53": 77, "54": 77, "55": 77, "56": 77, "57": 77, "58": 77, "59": 77, "60": 77, "61": 77, "62": 78, "63": 78, "64": 78, "65": 78, "66": 78, "67": 78, "68": 78, "69": 78, "70": 78, "71": 78, "72": 79, "73": 79, "74": 79, "75": 79, "76": 79, "77": 79, "78": 79, "79": 79, "80": 81, "81": 81, "82": 82, "83": 83, "84": 83, "85": 83, "86": 84, "87": 85, "88": 85, "89": 85, "90": 86, "91": 87, "92": 87, "93": 87, "94": 89, "95": 93, "101": 95, "102": 98, "103": 99, "104": 99, "110": 104}, "uri": "/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altaresultadopartido.mak"}
__M_END_METADATA
"""
