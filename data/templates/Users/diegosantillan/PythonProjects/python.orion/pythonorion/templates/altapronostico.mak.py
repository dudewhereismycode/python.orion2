# -*- coding:utf-8 -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
STOP_RENDERING = runtime.STOP_RENDERING
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 10
_modified_time = 1528238743.573069
_enable_loop = True
_template_filename = '/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altapronostico.mak'
_template_uri = '/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altapronostico.mak'
_source_encoding = 'utf-8'
from markupsafe import escape_silent as escape
_exports = []


def _mako_get_namespace(context, name):
    try:
        return context.namespaces[(__name__, name)]
    except KeyError:
        _mako_generate_namespaces(context)
        return context.namespaces[(__name__, name)]
def _mako_generate_namespaces(context):
    pass
def _mako_inherit(template, context):
    _mako_generate_namespaces(context)
    return runtime._inherit_from(context, 'local:templates.master', _template_uri)
def render_body(context,**pageargs):
    __M_caller = context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        _ = context.get('_', UNDEFINED)
        cantidad = context.get('cantidad', UNDEFINED)
        h = context.get('h', UNDEFINED)
        allrecords = context.get('allrecords', UNDEFINED)
        user = context.get('user', UNDEFINED)
        __M_writer = context.writer()
        __M_writer('\n\n<script>\n\n\n    function ingresapronostico() {\n        var arregloresultados = [];\n        var equipos = [];\n        for(var i=0;i<')
        __M_writer(escape(cantidad))
        __M_writer(';i++){\n                var resultadolocal=document.getElementById("pronosticoL"+i).value;\n                var teamlocal=document.getElementById("pronosticoteamL"+i).innerText;\n\n                var resultadovisita=document.getElementById("pronosticoV"+i).value;\n                var teamvisita=document.getElementById("pronosticoteamV"+i).innerText;\n\n                var lugarmexico=document.getElementById("orion_lugarmexico").value;\n\n\n                arregloresultados.push(resultadolocal);\n                arregloresultados.push(resultadovisita);\n                equipos.push(teamlocal);\n                equipos.push(teamvisita);\n        }\n\n        var confirma=confirm("¿Estas seguro de guardar tu pronostico?");\n        if(confirma){\n            $.ajax({\n                    type: "GET",\n                    url: "')
        __M_writer(escape(h.url()))
        __M_writer('/alta/pronosticopartido?equipos=" + equipos + "&resultados=" + arregloresultados+"&user=')
        __M_writer(escape(user))
        __M_writer('&lugarmexico="+lugarmexico,\n                    contentType: "application/json; charset=utf-8",\n                    data: {},\n                    success: function (parameterdata) {\n                        //Insert HTML code\n                        alert(parameterdata.error);\n\n                    },\n                    error: function () {\n                        alert("ERROR");\n                    },\n                    complete: function () {\n                    }\n            });\n        }\n      }\n\n</script>\n\n')

        i=0
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['i'] if __M_key in __M_locals_builtin_stored]))
        __M_writer('\n\n\n')
        for item in allrecords:
            if item.habilitado==1:
                __M_writer('\n    <p> <input type="text" id=\'pronosticoL')
                __M_writer(escape(i))
                __M_writer('\' size=10> <a id="pronosticoteamL')
                __M_writer(escape(i))
                __M_writer('"> ')
                __M_writer(escape(item.nombrelocal))
                __M_writer(' </a>\n        ')
                __M_writer(escape(_('vs')))
                __M_writer('  <a id="pronosticoteamV')
                __M_writer(escape(i))
                __M_writer('"> ')
                __M_writer(escape(item.nombrevisita))
                __M_writer(' </a> <input type="text" id=\'pronosticoV')
                __M_writer(escape(i))
                __M_writer("' size=10>\n\n    </p>\n\n    ")

                i=i+1
                
                
                __M_locals_builtin_stored = __M_locals_builtin()
                __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['i'] if __M_key in __M_locals_builtin_stored]))
                __M_writer('\n')
        __M_writer('\n')
        __M_writer(escape(_('Position of Mexico: ')))
        __M_writer('\n<select id="orion_lugarmexico" >\n      <option>1</option>\n      <option>2</option>\n      <option>3</option>\n      <option>4</option>\n\n</select>\n<br>\n<br>\n <button type="button"  onclick="ingresapronostico();" class="btn btn-primary">')
        __M_writer(escape(_('SAVE')))
        __M_writer('</button>')
        return ''
    finally:
        context.caller_stack._pop_frame()


"""
__M_BEGIN_METADATA
{"source_encoding": "utf-8", "uri": "/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altapronostico.mak", "line_map": {"64": 57, "65": 57, "66": 57, "67": 57, "68": 57, "69": 61, "75": 63, "76": 66, "77": 67, "78": 67, "79": 77, "80": 77, "86": 80, "28": 0, "38": 1, "39": 9, "40": 9, "41": 29, "42": 29, "43": 29, "44": 29, "45": 48, "51": 50, "52": 53, "53": 54, "54": 55, "55": 56, "56": 56, "57": 56, "58": 56, "59": 56, "60": 56, "61": 57, "62": 57, "63": 57}, "filename": "/Users/diegosantillan/PythonProjects/python.orion/pythonorion/templates/altapronostico.mak"}
__M_END_METADATA
"""
