#Envia las unidades que se han instalado de Urbvan
from sqlalchemy import *
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from pythonorion.model import PromoFiles
import requests
import time, datetime

import arrow


mydate = datetime.datetime.utcnow()
utc = arrow.get(mydate)
mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

#CONECCION A LA BASE DE DATOS
some_engine = create_engine('mysql://gpscontrol:qazwsxedc@db/orion')

Session = sessionmaker(bind=some_engine)
DBsession = Session()
Base = declarative_base()

promos = DBsession.query(PromoFiles).filter_by(internal_id=2).all()
for item in promos:
    if item.start <= mydate and item.expiration >= mydate:
        if item.notification is True and item.sended is False:
            params2 = "?keycode=2b4891ce&type=1&message="+item.noti_title+"&url=" + item.noti_message
            url = "https://api4sun.dudewhereismy.com.mx/mobile/appgpscontrol/allapp"  + params2
            request = requests.post(url)
            item.sended = True
            DBsession.commit()
    else:
        if item.expiration <= mydate:
            DBsession.delete(item)
            DBsession.commit()