<%doc><script src="${tg.url('/js/jquery-1.11.1.min.js')}"></script>
<script src="${tg.url('/js/alert.js')}"></script></%doc>

<script>


    function ingresapronostico() {
        var arregloresultados = [];
        var equipos = [];
        for(var i=0;i<${cantidad};i++){
                var resultadolocal=document.getElementById("pronosticoL"+i).value;
                var teamlocal=document.getElementById("pronosticoteamL"+i).innerText;

                var resultadovisita=document.getElementById("pronosticoV"+i).value;
                var teamvisita=document.getElementById("pronosticoteamV"+i).innerText;

                arregloresultados.push(resultadolocal);
                arregloresultados.push(resultadovisita);
                equipos.push(teamlocal);
                equipos.push(teamvisita);
        }

        var nombre=document.getElementById("orion_nameofclient").value;
        var telefono=document.getElementById("orion_phone").value;

        if(nombre==" " || telefono==" " || nombre=="" || telefono==""){
            alert("El nombre y el telefono son obligatorios");
        }
         else {

            var confirma = confirm("¿Estas seguro de guardar tu pronostico?");
            if (confirma) {
                $.ajax({
                    type: "GET",
                    url: "${h.url()}/alta/pronosticopartido?equipos=" + equipos + "&resultados=" + arregloresultados + "&user=${user}&nombre=" + nombre + "&telefono="
                    + telefono,
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (parameterdata) {
                        //Insert HTML code
                        alert(parameterdata.error);
                        location.reload();

                    },
                    error: function () {
                        alert("Asegurate que los marcadores sean correctos");
                    },
                    complete: function () {
                    }
                });
            }
        }
      }

</script>
<body style="background-color: #8a6d3b">

%if poder == 0:
%if estado!='yano':

<%
    i=0
    %>
<h3>PRONOSTICO DE FUTBOL</h3>
    <br>
    NOMBRE: <input type="text" id="orion_nameofclient" size="30" required>&nbsp;&nbsp;&nbsp;
    TELEFONO: <input type="text" id="orion_phone" required>
    <br>
    <br>
    <p>TU PRONOSTICO</p>
    <p>Introduce un marcador para cada partido</p>


      <table style="width: 500px;text-align: center;" >
              <tr style="text-align: center">
                <th width="20px">Goles Local</th>
                <th width="20px"></th>
                <th width="20px" style="text-align: center">Local</th>
                  <td width="20px"></td>
                <th width="20px"></th>
                <th width="20px"></th>
                <th width="20px" style="text-align: center">Visita</th>
                  <td width="20px"></td>
                <th width="20px">Goles Visita</th>
              </tr>
% for item in allrecords:
    %if item.habilitado==1:

    <tr width="20px" style="text-align: center">
        <td width="20px"><input type="number" id='pronosticoL${i}' style="width:4em;"> </td>
        <td width="20px"></td>
        <td width="20px"><a id="pronosticoteamL${i}"> ${item.nombrelocal} </a></td>
        <td width="20px"></td>
        <td width="20px"> ${_('vs')} </td>
        <th width="20px"></th>
        <td width="20px"><a id="pronosticoteamV${i}"> ${item.nombrevisita} </a> </td>
        <td width="20px"></td>
        <td width="20px"><input type="number" id='pronosticoV${i}' style="width:4em;"></td>
    </tr>
    <%
    i=i+1
    %>
%endif
% endfor
      </table><br>

<br>
       <a onclick="window.open('https://orion.dudewhereismy.com.mx/rules');"> REGLAS DE <i>LA QUINIELA </i></a>
<br><br>
 <button type="button"  onclick="ingresapronostico();" class="btn btn-primary">GUARDAR</button>
        %else:
        <h1>YA NO PUEDES METER PRONOSTICOS</h1>
%endif
    %else:
<%
con=0
%>
    <h3 onclick="window.open('https://orion.dudewhereismy.com.mx/rules');"><a> GANADORES</a> </h3>
    <h3 onclick="window.open('https://orion.dudewhereismy.com.mx/rules');"><a> REGLAS DE <i>LA QUINIELA </i></a> </h3>
    <h3>${_('Tus Pronosticos')}</h3>
    <br>

    <table class="table" style="width:30%">
          <tr>
            <td>${_('USUARIO')}</td>
             <td>${_('PUNTOS')}</td>
              <td>${_('POSITION')}</td>
        </tr>

        <tr>
            <td>${user}</td>
            <td>${puntos}</td>
            <td>${posicion}</td>
        </tr>

    </table>
    <table class="table" style="width:80%">
            <tr>

            <th scope="col" style="color: darkgray">${_('Partido')}</th>
            <th scope="col" style="color: darkgray">${_('Goles Local')}</th>
            <th scope="col" style="color: darkgray">${_('Goles Visit')}</th>

            </tr>
            % for item1 in listalias:

            <tr>

                <td style="color: white">${item1}</td>
                <td style="color: white">${listlocal[con]}</td>
                <td style="color: white">${listvisita[con]}</td>



            </tr>
                <%
                con=con+1
                %>
        %endfor
        </table>

<br>
     <table class="table" style="width:30%">
         <tr>
         <td style="color: white">${_('POSICION DE MEXICO')}</td>
             <td style="color: white">${lugarprediccion}</td>
         </tr>
     </table>


%endif

</body>
