<script>

    function savePromoFileOrion(){
        var input = document.querySelector('input[type=file]');
        file = input.files[0];
        var expiration = $('#expiration_promo_file_orion').val();
        var start = $('#start_promo_file_orion').val();
        var url = $('#expiration_url_orion').val();

        var noti_title = $('#expiration_noti_title_orion').val();
        var noti_message = $('#expiration_noti_message_orion').val();

        var send = true;
        console.log("ENTRE");
        console.log($('#expiration_noti_orion').is(":checked"));
        if ($('#expiration_noti_orion').is(":checked")) {
            if(noti_title === ""){
                console.log("ENTRE noti_title");
                $.alert("${_('Titulo de notificación obligatorio')}",{type: "warning"});
                send = false;
            }
            if(noti_message === ""){
                console.log("ENTRE noti_message");
                $.alert("${_('Mensaje de notificación obligatorio')}",{type: "warning"});
                send = false;
            }
        }else{
            console.log("Olvidaste checar notificación");
            if(noti_title !== "" || noti_message !== ""){
                console.log("ENTRE noti_title");
                $.alert("${_('Seleccione que es notificación o borre titulo y mensaje de notificación')}",{type: "warning"});
                send = false;
            }

        }

        if(send === true){
            var formData = new FormData();
            formData.append("image", file);
            formData.append("expiration", expiration);
            formData.append("start", start);
            formData.append("url", url);
            formData.append("notification",$('#expiration_noti_orion').is(":checked"));
            formData.append("noti_title", noti_title);
            formData.append("noti_message", noti_message);
            formData.append("user", "${kw['user']}");
            formData.append("application_id", "${kw['application_id']}");
            formData.append("internal_id", "${kw['internal_id']}");
            var request = new XMLHttpRequest();
            request.open("POST", '${h.url()}/promos/savepromo');
            request.send(formData);
            request.onload  = function() {
                var response = JSON.parse(request.responseText);
                if (response.error == "ok"){
                    $.alert("${_('Promo File Saved')}",{type: "success"});
                    closeSunWindow();
                }else{
                    $.alert(response.error,{type: "warning"});
                }
            }
        }

    }
</script>
<body onload="entreOrionUploadFile()">
<form id="addPromoFileOrion" >
    <fieldset>
        <table style="width:100%">
            <tr>
                <td style="width:30%">
                    ${_('Promo Start')}:
                </td>
                <td>
                    <input type="datetime-local" id="start_promo_file_orion" value="${kw['now']}" required>
                </td>
            </tr>
             <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Promo Expiration')}:
                </td>
                <td>
                    <input type="datetime-local" id="expiration_promo_file_orion" value="${kw['now']}" required>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Url')}:
                </td>
                <td>
                    <input type="text" id="expiration_url_orion" style="width: 100%">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Add File')}:
                </td>
                <td >
                    <input type="file" id="promo_file_orion" required>
                </td>
            </tr>
            <tr style="height: 30px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    <b>${_('Notification')}</b>
                </td>
                <td >
                    <input type="checkbox" id="expiration_noti_orion"><label for="expiration_noti_orion">&nbsp;</label>
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Title')}:
                </td>
                <td >
                    <input type="text" id="expiration_noti_title_orion" maxlength="50" style="width: 100%">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Message')}:
                </td>
                <td >
                    <input type="text" id="expiration_noti_message_orion" maxlength="200" style="width: 100%">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
        </table>
    </fieldset>
</form>
<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="savePromoFileOrion()" style="padding:5px;float: right"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;${_('Save')}</button>

</body>

