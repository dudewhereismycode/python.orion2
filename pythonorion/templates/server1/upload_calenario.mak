<script>
    function savePromoFileOrion(){
        var input = document.querySelector('input[type=file]');
        file = input.files[0];
        var expiration = $('#expiration_calendar_file_orion').val();
        var start = $('#start_calendar_file_orion').val();
        var url = $('#expiration_url_orion').val();
        var formData = new FormData();
        formData.append("image", file);
        formData.append("expiration", expiration);
        formData.append("start", start);
        formData.append("url", url);
        formData.append("user", "${kw['user']}");
        formData.append("application_id", "${kw['application_id']}");
        formData.append("internal_id", "${kw['internal_id']}");
        var request = new XMLHttpRequest();
        request.open("POST", '${h.url()}/manual/savepromo');
        request.send(formData);
        request.onload  = function() {
            var response = JSON.parse(request.responseText);
            if (response.error == "ok"){
                $.alert("${_('Promo File Saved')}",{type: "success"});
                closeSunWindow();
            }else{
                $.alert(response.error,{type: "warning"});
            }
        }
    }
</script>
<form id="addPromoFileOrion">
    <fieldset>
        <table style="width:100%">
            <tr>
                <td style="width:30%">
                    ${_('Promo Start')}:
                </td>
                <td>
                    <input type="datetime-local" id="start_calendar_file_orion" >
                </td>
            </tr>
             <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Promo Expiration')}:
                </td>
                <td>
                    <input type="datetime-local" id="expiration_calendar_file_orion" >
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Url')}:
                </td>
                <td>
                    <input type="text" id="expiration_url_orion" style="width: 100%">
                </td>
            </tr>
            <tr style="height: 10px">
                <td></td>
            </tr>
            <tr>
                <td style="width:30%">
                    ${_('Add File')}:
                </td>
                <td >
                    <input type="file" id="calendar_file_orion">
                </td>
            </tr>
            <tr style="height: 100px">
                <td></td>
            </tr>
        </table>
    </fieldset>
</form>
<button class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" onclick="savePromoFileOrion()" style="padding:5px;float: right"><span class="glyphicon glyphicon-floppy-disk"></span>&nbsp;${_('Save')}</button>

