
<script>
    $(window).on("resize", function () {
        var $gridAppRenovation = $("#jqGridAppRenovation"),
                newWidthAppRenovation = $gridAppRenovation.closest(".ui-jqgrid").parent().width();
            $gridAppRenovation.jqGrid("setGridWidth", newWidthAppRenovation, true);
    });
    var grid_nameAppRenovation = '#jqGridAppRenovation';
    var grid_pagerAppRenovation= '#listPagerAppRenovation';
    var update_urlAppRenovation='https://orion.dudewhereismy.com.mx/promos/loadRenovationsGrid?appid=${kw['appid']}';
    var load_urlAppRenovation  ='https://orion.dudewhereismy.com.mx/promos/loadRenovationsGrid?appid=${kw['appid']}';
    var addParamsAppRenovation = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlAppRenovation,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsAppRenovation = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlAppRenovation,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsAppRenovation = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlAppRenovation,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsAppRenovation = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlAppRenovation,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsAppRenovation = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlAppRenovation,modal: true};
    var gridAppRenovation = jQuery(grid_nameAppRenovation);
    $(document).ready(function () {
                gridAppRenovation.jqGrid({
                url: load_urlAppRenovation,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Status')}', '${_('# Device')}', '${_('Imei')}', '${_('Tel')}','${_('CCID')}','${_('ECO')}','${_('Brand')}','${_('Model')}'
                ,'${_('Vin')}','${_('Color')}','${_('Plates')}','${_('Application')}','${_('Instalation Date')}','${_('Attendant')}'
                ,'${_('User')}','${_('Password')}','${_('Process')}','${_('Observations')}','${_('Email')}','${_('Tel 2')}'
                ,'${_('Installer')}','${_('Review Date')}','${_('Reviwe Inst')}','${_('review2_date')}','${_('reviwe2_inst')}','${_('review3_date')}'
                ,'${_('reviwe3_inst')}','${_('phone')}','${_('last_report')}','${_('device')}','${_('year')}','${_('client_id')}'
                ,'${_('internal_id')}','${_('server')}','${_('application_id')}','${_('Created')}','${_('Route')}','${_('')}','${_('GPS')}'
                ,'${_('Camera')}','${_('Panic Button')}','${_('Adscription')}','${_('Zone')}','${_('Ticket')}','${_('Semov')}'
                ,'${_('Instala Date Jess')}','${_('Server1 Created')}','${_('Instalation Year')}','${_('Instalation Month')}','${_('Instalation Day')}','${_('Last Renovation')}','${_('Payment Type')}','${_('R. Status')}'],
                colModel: [
                    {name: 'id',index: 'id', align: 'center',key:true,hidden: true, editable: false,edittype: 'text',editrules: {required: true}},
                    {name: 'status', index: 'status',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'no_device', index: 'no_device',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'imei', index: 'imei',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'tel', index: 'tel',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'ccid', index: 'ccid',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'eco', index: 'eco',align: 'left',hidden: false, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'brand', index: 'brand',align: 'left',hidden: true, editable: true, edittype: 'text', editrules: {required: false}},
                    {name: 'model',index: 'model',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'vin',index: 'vin',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'color',index: 'color',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'plates',index: 'plates',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'application_name',index: 'application_name',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'instalation_date',index: 'instalation_date',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'attendant',index: 'attendant',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'user',index: 'user',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'password',index: 'password',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'process',index: 'process',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'observations',index: 'observations',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'email',index: 'email',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'tel2',index: 'tel2',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'installer',index: 'installer',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'review_date',index: 'review_date',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'reviwe_inst',index: 'reviwe_inst',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'review2_date',index: 'review2_date',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'reviwe2_inst',index: 'reviwe2_inst',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'review3_date',index: 'review3_date',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'reviwe3_inst',index: 'reviwe3_inst',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'phone',index: 'phone',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'last_report',index: 'last_report',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'device',index: 'device',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'year',index: 'year',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'client_id',index: 'client_id',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'internal_id',index: 'internal_id',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'server',index: 'server',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'application_id',index: 'application_id',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'created',index: 'created',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'route',index: 'route',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'principal_phone',index: 'principal_phone',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'gps',index: 'gps',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'number_cam',index: 'number_cam',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'panic_button',index: 'panic_button',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'adscription',index: 'adscription',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'zone',index: 'zone',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'ticket',index: 'ticket',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'semov',index: 'semov',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'jess_instala_date',index: 'jess_instala_date',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'server_created',index: 'server_created',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'instalation_year',index: 'instalation_year',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'instalation_month',index: 'instalation_month',align: 'left',hidden: false,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'instalation_day',index: 'instalation_day',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'last_renovation',index: 'last_renovation',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'payment_type',index: 'payment_type',align: 'left',hidden: true,editable: true,edittype: 'text',editrules: {required: false}},
                    {name: 'renovation_status',index: 'renovation_status',align: 'left',hidden: false,editable: true,edittype: 'text',editrules: {required: false}},
                ],
                pager: jQuery(grid_pagerAppRenovation),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,
                    gridComplete: function () {
                    var count=gridAppRenovation.jqGrid('getGridParam', 'records');
                    if (count >0){
                        % if image:
                            % if video:
                                document.getElementById("videoPromo").style.display = "none";
                            % else:
                                document.getElementById("imagePromo").style.display = "none";
                            % endif
                        % else:
                            document.getElementById("defaultPromo").style.display = "none";
                        % endif
                        document.getElementById("RenovationsGrid").style.display = "block";
                    }
                 }
            });
            gridAppRenovation.jqGrid('navGrid',grid_pagerAppRenovation,{edit:false,add:false,del:false, search:false},
                            editParamsAppRenovation,
                            addParamsAppRenovation,
                            deleteParamsAppRenovation,
                            searchParamsAppRenovation,
                            viewParamsAppRenovation);
            });
    $.extend($.jgrid.nav,{alerttop:1});
</script>
% if image:
    % if video:
        <video width="50%" id="videoPromo" autoplay controls>
          <source src="data:video/mp4;base64,${kw['img']}" type="video/mp4">
          <source src="data:video/mov;base64,${kw['img']}" type="video/mov">
          Tu navegador no soporta videos HTML5.
        </video>
    % else:
       <a title="PMO" ${kw['url']} target="_blank"><img style="width:50%;height:50%;" id="imagePromo" src="data:image/jpeg;base64,${kw['img']}"/></a>
    % endif
% else:
    <img style="width:50%;height:50%;alignment: center;" src="https://orion.dudewhereismy.com.mx/img/promodefault.jpg" id="defaultPromo"alt="Promotion">
% endif
<div id="RenovationsGrid" style="display:none;">
<table style="width:100%">
    <table id="jqGridAppRenovation" class="scroll" cellpadding="0" cellspacing="0" ></table>
    <div id="listPagerAppRenovation" class="scroll" style="text-align:center;"></div>
</table>
</div>