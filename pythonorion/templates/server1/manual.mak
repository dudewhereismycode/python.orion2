<!DOCTYPE html>
<html>
<title>GPScontrol VideoManuales</title>
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">-->

<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<meta name="viewport" content="width=device-width, initial-scale=1">

<style>
body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
  background-color: #15273F;
}

.topnav {
  overflow: hidden;
  background-color: #15273F;
  width: 100%;
}

.topnav a {

  float: left;
  display: block;
  color: #f2f2f2;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 25px;

}

.topnav a:hover {
  background-color: #6F8DAF;
  color: black;
}

.topnav a.active {
  color: white;
}

.topnav .icon {
  display: none;
}

.main {
  padding: 16px;
  padding-bottom: 30px;
  margin-top: 10px;
  height: 1500px; /* Used in this example to enable scrolling */
}

@media screen and (max-width: 600px) {
  .topnav a:not(:first-child) {display: none;}
  .topnav a.icon {
    float: right;
    display: block;
  }
}

@media screen and (max-width: 600px) {
  .topnav.responsive {position: relative;}
  .topnav.responsive .icon {
    position: absolute;
    right: 0;
    top: 0;
  }
  .topnav.responsive a {
    float: none;
    display: block;
    text-align: left;
  }
}

.ir-arriba {
  position: fixed;
  text-align: right;
}

</style>

<script>
function myFunction() {
  var x = document.getElementById("myTopnav");
  if (x.className === "topnav") {
    x.className += " responsive";
  } else {
    x.className = "topnav";
  }
}

function scrollWin() {
  window.scrollTo(0,0);
}

</script>

<body >
<!--    <div class="topnav" id="myTopnav">-->
<!--        <a href="#home" class="active">Menu</a>-->
<!--        <a href="#ticket" >Crear Ticket</a>-->
<!--        <a href="#vehiculo" >Información del Vehículo</a>-->
<!--        <a href="#contactos" >Contactos de Emergencia</a>-->
<!--        <a href="#chekdo" >CHEKDO</a>-->
<!--        <a href="javascript:void(0);" class="icon" onclick="myFunction()">-->
<!--            <b class="fa fa-bars">&#9776;</b>-->
<!--        </a>-->
<!--    </div>-->
     <div style="padding-left:15px" id="main" class="main">
         <div id="app" class="w3-container" style="background-color: #274061;color:#FFFFFF; border-radius: 10px">
          <h2>¡Lleva tu flota a donde vayas! </h2>
          <a href="https://orion.dudewhereismy.com.mx/img/video/app-conocela.mp4" target="_blank">
              <img src="https://orion.dudewhereismy.com.mx/img/video/app2.png" style="width: 100%;height: auto;">
          </a>

<!--          <video style="width:100%;" controls>-->
<!--          <source src="https://www.youtube.com/embed/CEhiSewyH4U" type="video/mp4">-->
<!--          Your browser does not support HTML5 video.-->
<!--        </video>-->
<!--        <iframe width="100%" height="780" src="https://www.youtube.com/embed/CEhiSewyH4U" title="YouTube video player"-->
<!--                frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"-->
<!--                allowfullscreen></iframe>-->

          <div class="w3-container">
            <p>Imagina sentir la tranquilidad de saber todo lo que sucede con tus unidades, únicamente ingresando desde tu móvil y a la hora que lo necesites.<br>
            ¡Más Control, más Poder!<br>
            Disponible para Android / iOS
            </p>
          </div>
        </div>
        <div id="ticket"><br></div>
        <div class="w3-container" style="background-color: #274061;color:#FFFFFF; border-radius: 10px">
          <h2>Nuevo Ticket</h2>
          <a href="https://orion.dudewhereismy.com.mx/img/video/crearticket.mp4" target="_blank">
              <img src="https://orion.dudewhereismy.com.mx/img/video/newticket.png" style="width: 100%;height: auto;">
          </a>
<!--          <video style="width:100%;" controls>-->
<!--          <source src="https://orion.dudewhereismy.com.mx/img/video/crearticket.mp4" type="video/mp4">-->
<!--          Your browser does not support HTML5 video.-->
<!--        </video>-->
          <div class="w3-container">
            <p>Levanta un ticket de Soporte Técnico sin salir de tu plataforma. En menos de 48hr nuestros especialistas te estarán contactando para darte una respuesta y solución a tu ticket.</p>
          </div>
        </div>

        <div id="vehiculo"><br></div>
        <div class="w3-container" style="background-color: #274061;color:#FFFFFF; border-radius: 10px" >
          <h2>Información del Vehículo</h2>
          <a href="https://orion.dudewhereismy.com.mx/img/video/informacionVehicular.mp4" target="_blank">
              <img src="https://orion.dudewhereismy.com.mx/img/video/informaciondelvehiculo.png" style="width: 100%;height: auto;">
          </a>
<!--          <video style="width:100%" controls>-->
<!--          <source src="https://orion.dudewhereismy.com.mx/img/video/informacionVehicular.mp4" type="video/mp4">-->
<!--          Your browser does not support HTML5 video.-->
<!--          </video>-->
        <div class="w3-container">
            <p>Te pedimos tener actualizada la información de tus vehículos para que al momento de activarse un
                Protocolo de Emergencia nuestro Centro de Monitore tenga la información completa y se pueda tener una reacción inmediata.</p>
        </div>
        </div>

        <div id="contactos"><br></div>
        <div class="w3-container" style="background-color: #274061;color:#FFFFFF; border-radius: 10px" >
          <h2>Contactos de Emergencia</h2>
         <a href="https://orion.dudewhereismy.com.mx/img/video/contactosdeemergencia.mp4" target="_blank">
             <img src="https://orion.dudewhereismy.com.mx/img/video/contactosdeemergencia.png" style="width: 100%;height: auto;">
         </a>
<!--         <video style="width:100%" controls>-->
<!--          <source src="https://orion.dudewhereismy.com.mx/img/video/contactosdeemergencia.mp4" type="video/mp4">-->
<!--          Your browser does not support HTML5 video.-->
<!--        </video>-->
          <div class="w3-container">
            <p>Para nosotros es importante contar con los contactos actualizados del pesonal que puede recibir notificaciónes de tus unidades, ya sea para promociones, seguimientos de tickets,
                emergencias, alertas, o saber tu personal de confianza. No olvides dar de baja a las personas que ya no se encuentren laborando contigo mediante un correo a soporte@gpscontrol.com</p>
        </div>
        </div>

        <div  id="chekdo"><br></div>
        <div class="w3-container" style="background-color: #274061;color:#FFFFFF; border-radius: 10px">
            <h2>CHEKDO</h2>
            <a href="https://orion.dudewhereismy.com.mx/img/video/Checkdo_Plataforma.mp4" target="_blank">
                <img src="https://orion.dudewhereismy.com.mx/img/video/checkdo.png" style="width: 100%;height: auto;">
            </a>
<!--            <video style="width:100%" controls>-->
<!--                <source src="https://orion.dudewhereismy.com.mx/img/video/Checkdo_Plataforma.mp4" type="video/mp4">-->
<!--                Your browser does not support HTML5 video.-->
<!--            </video>-->
            <div class="w3-container">
                <p>Ideal para la supervisión del cumplimiento de los trabajos en campo como: toma de pedidos, puntos de trabajo, distribución, entregas, etc.</p>
                <br><br><br>
            </div>
        </div>
    </div>
</body>
</html>

