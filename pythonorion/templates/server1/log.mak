<script>
    $(window).on("resize", function () {
            var $gridOrionLog = $("#jqGridOrionLog"),
                newWidth = $gridOrionLog.closest(".ui-jqgrid").parent().width();
                $gridOrionLog.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameOrionLog = '#jqGridOrionLog';
    var grid_pagerOrionLog= '#listPagerOrionLog';
    var update_urlOrionLog='${h.url()}/promos/update_log?appID=${application_id}&internal_id=${internal_id}';
    var load_urlOrionLog  ='${h.url()}/promos/load_log?appID=${application_id}&internal_id=${internal_id}';
    var addParamsOrionLog = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlOrionLog,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsOrionLog = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlOrionLog,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsOrionLog = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOrionLog,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsOrionLog = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOrionLog,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsOrionLog = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlOrionLog,modal: true};

    var gridOrionLog = jQuery(grid_nameOrionLog);
            $(document).ready(function () {
                gridOrionLog.jqGrid({
                url: load_urlOrionLog,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('App ID')}','${_('Counter')}', '${_('First Log')}', '${_('Last Log')}'],
                colModel: [
                    {name: 'id',index: 'id', align: 'center',key:true,hidden: true,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'app_id',index: 'app_id', align: 'left',hidden:false ,editable: true, edittype: 'string',editrules: {required: false}},
                    {name: 'counter',index: 'counter', align: 'left',hidden:false ,editable: true, edittype: 'string',editrules: {required: false}},
                    {name: 'first_login', index: 'first_login',align: 'left',hidden: false, editable: true, edittype: 'string', editrules: {required: true}},
                    {name: 'last_login',index: 'last_login', align: 'left',hidden: false,editable: true, edittype: 'string',editrules: {required: true}},
                ],
                pager: jQuery(grid_pagerOrionLog),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,

            });
            gridOrionLog.jqGrid('navGrid',grid_pagerOrionLog,{edit:false,add:false,del:false,search:true},
                            editParamsOrionLog,
                            addParamsOrionLog,
                            deleteParamsOrionLog,
                            searchParamsOrionLog,
                            viewParamsOrionLog);


            });
            $.extend($.jgrid.nav,{alerttop:1});


    function refreshOrionLog(){
        $.ajax({
            type: "GET",
            url: "${h.url()}/promos/refresh",
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                $.alert("${_('Done')}!",{type:"success"});
                gridOrionLog.trigger( 'reloadGrid' );
            },
            error: function () {
                alert("Asegurate que los marcadores sean correctos");
            },
            complete: function () {
            }
        });
    }
</script>
</div>
    <!-- page start-->
<button type="button"  onclick="refreshOrionLog();" class="ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" style="padding:5px;margin-right:20px;">Refresh</button><br><br>
<div id="dialogOrionLog"  title="${_('Log')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridOrionLog" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerOrionLog" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->