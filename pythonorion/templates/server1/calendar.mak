<!DOCTYPE html>
<html>
<head>
<style>
* {box-sizing: border-box;}
ul {list-style-type: none;}

.month {
  padding: 70px 25px;
  width: 100%;
  background: #1abc9c;
  text-align: center;
}

.month ul {
  margin: 0;
  padding: 0;
}

.month ul li {
  color: white;
  font-size: 20px;
  text-transform: uppercase;
  letter-spacing: 3px;
}

.month .prev {
  float: left;
  padding-top: 10px;
}

.month .next {
  float: right;
  padding-top: 10px;
}

.weekdays {
  margin: 0;
  padding: 10px 0;
  background-color: #ddd;
}

.weekdays li {
  display: inline-block;
  width: 13.6%;
  color: #666;
  text-align: center;
}

.days {
  padding: 10px 0;
  background: #eee;
  margin: 0;
}

.days li {
  list-style-type: none;
  display: inline-block;
  width: 13.6%;
  text-align: center;
  margin-bottom: 5px;
  font-size:12px;
  color: #777;
}

.days li .active {
  padding: 5px;
  background: #1abc9c;
  color: white !important
}

/* Add media queries for smaller screens */
@media screen and (max-width:720px) {
  .weekdays li, .days li {width: 13.1%;}
}

@media screen and (max-width: 420px) {
  .weekdays li, .days li {width: 12.5%;}
  .days li .active {padding: 2px;}
}

@media screen and (max-width: 290px) {
  .weekdays li, .days li {width: 12.2%;}
}
</style>
</head>
<body>

<div class="month">
  <ul>
    <li>${kw['month']}<br>
      <span style="font-size:18px">${kw['year']}</span>
    </li>
  </ul>
</div>

<ul class="weekdays">
  <li>Mo</li>
  <li>Tu</li>
  <li>We</li>
  <li>Th</li>
  <li>Fr</li>
  <li>Sa</li>
  <li>Su</li>
</ul>

<ul class="days">
    % for item in kw['html']:
        <li><span class="${item['class']}">${item['data']}</span><br>
            % if item['data'] in kw:
                % if kw[item['data']]['video']:
                        <video width="50%" autoplay controls>
                          <source src="data:video/mp4;base64,${kw[item['data']]['img']}" type="video/mp4">
                          <source src="data:video/mov;base64,${kw[item['data']]['img']}" type="video/mov">
                          Tu navegador no soporta videos HTML5.
                        </video>
                    % else:
                       <a title="PMO" ${kw[item['data']]['url']} target="_blank"><img style="width:50%;height:50%;" src="data:image/jpeg;base64,${kw[item['data']]['img']}"/></a>
                    % endif
            % else:
                <img style="width:50%;height:50%;alignment: center;" src="https://orion.dudewhereismy.com.mx/img/img/promodefault.jpg" alt="Promotion">
            % endif

        </li>
    % endfor
</ul>

</body>
</html>