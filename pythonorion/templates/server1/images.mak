<script>
    $(window).on("resize", function () {
            var $gridOrionImages = $("#jqGridOrionImages"),
                newWidth = $gridOrionImages.closest(".ui-jqgrid").parent().width();
                $gridOrionImages.jqGrid("setGridWidth", newWidth, true);
            });
    var grid_nameOrionImages = '#jqGridOrionImages';
    var grid_pagerOrionImages= '#listPagerOrionImages';
    var update_urlOrionImages='${h.url()}/promos/update_image?appID=${application_id}&internal_id=${internal_id}';
    var load_urlOrionImages  ='${h.url()}/promos/load_image?appID=${application_id}&internal_id=${internal_id}';
    var addParamsOrionImages = {left: 0,width: window.innerWidth-700,top: 20,height: 190,url: update_urlOrionImages,mtype: 'GET', closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var editParamsOrionImages = {left: 0,width: window.innerWidth-700,top: 20,height: 200,url: update_urlOrionImages,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,modal: true, width: "500",editfunc: function (rowid) {} };
    var deleteParamsOrionImages = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOrionImages,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var viewParamsOrionImages = {left: 0,width: window.innerWidth-700,top: 20,height: 130,url: update_urlOrionImages,mtype: 'GET',closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true};
    var searchParamsOrionImages = {top: 20,height: 130,width: "500",closeAfterAdd: true,closeAfterEdit: true,closeAfterSearch:true,url: update_urlOrionImages,modal: true};
    function formatImage(cellValue, options, rowObject) {
            var imageHtml = '<img src="data:image/png;base64,'+cellValue+'" width="150" height="80" />';
            return imageHtml;
        }
    function formatNoti(cellValue, options, rowObject) {
        console.log(cellValue);
        var imageHtml = "<span class=\"glyphicon glyphicon-remove-circle btn-lg\" style=\"color: darkred;font-size: 20px;\" ></span>";
        if(cellValue === true){
            imageHtml = "<span class=\"glyphicon glyphicon-ok-circle btn-lg\" style=\"color: darkgreen;font-size: 20px;\"></span>";
        }
        return imageHtml;
        }
    var gridOrionImages = jQuery(grid_nameOrionImages);
            $(document).ready(function () {
                gridOrionImages.jqGrid({
                url: load_urlOrionImages,
                datatype: 'json',
                mtype: 'GET',
                colNames: ['${_('ID')}', '${_('Image')}','${_('Expiration')}', '${_('App ID')}',
                    '${_('Server1')}','${_('Internal')}','${_('Start')}','${_('Type')}','${_('Url')}',
                    '${_('Nofitication')}','${_('Title')}','${_('Message')}'
                ],
                colModel: [
                    {name: 'id',index: 'id', align: 'center',key:true,hidden: true,width:20, editable: false,edittype: 'text',editrules: {required: true},search:false},
                    {name: 'file',index: 'file', formatter: formatImage,align: 'center',hidden:false ,editable: true, edittype: 'string',editrules: {required: false}},
                    {name: 'expiration',index: 'expiration', align: 'center',width:60,hidden:false ,editable: true, edittype: 'string',editrules: {required: false}},
                    {name: 'application_id', index: 'application_id',align: 'left',hidden: true, editable: true, edittype: 'string', editrules: {required: true}},
                    {name: 'server1_id',index: 'server1_id', align: 'left',hidden: true,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'internal_id',index: 'internal_id', align: 'left',hidden: true,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'url',index: 'url', align: 'left',hidden: true,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'type',index: 'type', align: 'left',hidden: true,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'start',index: 'start', align: 'center',width:60,hidden: false},
                    {name: 'notification',index: 'notification', formatter: formatNoti,width:30, align: 'center',hidden: false,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'noti_title',index: 'noti_title', align: 'left',hidden: false,editable: true, edittype: 'string',editrules: {required: true}},
                    {name: 'noti_message',index: 'noti_message', align: 'left',hidden: false,editable: true, edittype: 'string',editrules: {required: true}},

                ],
                pager: jQuery(grid_pagerOrionImages),
                rowNum: 16,
                rowList: [16, 50, 100],
                sortname: 'id',
                sortorder: "asc",
                viewrecords: true,
                autowidth: true,
                height: 250,

            });
            gridOrionImages.jqGrid('navGrid',grid_pagerOrionImages,{edit:false,add:false,del:true,delcaption:'${_('Delete')}',deltitle:'${_('Delete')}', search:true},
                            editParamsOrionImages,
                            addParamsOrionImages,
                            deleteParamsOrionImages,
                            searchParamsOrionImages,
                            viewParamsOrionImages);


            });
            $.extend($.jgrid.nav,{alerttop:1});

            gridOrionImages.navButtonAdd(grid_pagerOrionImages, {
            buttonicon: "ui-icon-comment",
            title: "${_('Notify')}",
            caption: "${_('Notify')}",
            position: "first",
            onClickButton: function(){
                var rowid = $('#jqGridOrionImages').jqGrid('getGridParam', 'selrow');
                if(rowid != null){
                    testNofityOrion(rowid);
                }else{
                    $.alert("${_('No row selected')}", { autoClose:false,type: "warning"});
                }
            }
        });

    function testNofityOrion(rowId){
        $.ajax({
            type: "GET",
            url: '${h.url()}/promos/testnoti',
            contentType: "application/json; charset=utf-8",
            data: {
                'id':rowId
            },
            success: function(parameterdata) {

                if ($("#dialogOrionTestNotify").length){
                    $("#dialogOrionTestNotify").remove();
                }

                var newDiv = $(document.createElement('div'));
                var html = '<div id="dialogOrionTestNotify"></div>';
                newDiv.html(html);

                newDiv.html(parameterdata.dedwizardtemplate);
                var DedicatedWizardDialog = newDiv.dialog({
                    autoOpen: false,
                    title: "${_('Test Nofification')}",
                    height: 400,
                    width: 500,
                    modal: true,
                    buttons: {
                        "${_('Send')}": function() {
                            var phone = $('#nofityTest_notify').val();
                            var iOS = $('#nofityTest_iOS').is(":checked");
                            var android = $('#nofityTest_android').is(":checked");
                            console.log(phone.length);
                            console.log(iOS);
                            console.log(android);
                            if(iOS === true || android === true ){
                                if(phone.length === 10){
                                $.ajax({
                                    type: "GET",
                                    url: "${h.url()}/promos/sendNoti",
                                    contentType: "application/json; charset=utf-8",
                                    data: {
                                        'id':rowId,
                                        'phone':phone,
                                        'iOS':iOS,
                                        'android':android
                                    },
                                    success: function(data) {
                                        // data.value is the success return json. json string contains key value
                                        if(data.error == "ok"){
                                            $.alert("Notificación enviada", { autoClose:false,type: "success"});
                                        }else{
                                             $.alert(data.error, { autoClose:false,type: "warning"});
                                        }
                                    },
                                    error: function() {
                                        //alert("#"+ckbid);
                                        $.alert("${_('Error accessing to')} /promos/sendNoti", { autoClose:false,type: "danger"});
                                        return true;
                                    },
                                    complete: function() {
                                    }
                                });
                                //$('#CatalogsFormVenus')[0].reset();
                                newDiv.dialog( "close" );
                            }else{
                                    $.alert("Celular invalido", { autoClose:false,type: "warning"});
                                }
                            }else{
                                $.alert("Seleccione tipo de celular", { autoClose:false,type: "warning"});
                            }
                        },
                        "${_('Close')}": function() {
                            newDiv.dialog( "close" );
                        }
                    },
                    close: function() {
                        $(this).dialog('destroy').remove();
                        $('#jqGridOrionImages').trigger( 'reloadGrid' );
                    }
                });
                DedicatedWizardDialog.dialog( "open" );
            }, error: function() {
                $.alert("${_('Error accessing server')} /promos/testnoti",{type: "danger"});
            },
            complete: function() {
            }
        });
    }
</script>
</div>
    <!-- page start-->
<div id="dialogOrionImages"  title="${_('Images')}"></div>
    <!-- JQGRID table start-->
    <table style="width:100%">
    <table id="jqGridOrionImages" class="scroll" cellpadding="0" cellspacing="0"></table>
    <div id="listPagerOrionImages" class="scroll" style="text-align:center;"></div>
    </table>
    <br>
  <!-- page end-->