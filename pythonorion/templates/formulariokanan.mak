<%inherit file="local:templates.master"/>

<%def name="title()">
  Orion
</%def>

<style>

    body{
        background-color: #AA151C;

    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>


    $(function () {
        $('#formulariokanan').on('submit', function (e) {
            e.preventDefault();
            var formulario = new FormData($(this)[0]);
            //alert(formulario);
            $.ajax({
                type: "POST",
                url: "${h.url()}/registrodekanandatos",
                processData: false,  // tell jQuery not to process the data
                contentType: false ,  // tell jQuery not to set contentType
                data: formulario,

                    beforeSend: function() {
                        document.getElementById("divmensaje").innerHTML="";
                        document.getElementById("divmensaje").innerHTML="<img src=\"${h.url()}/img/ajax-loader.gif\" >";
                    },
                success: function (parameterdata) {
                    //alert("succes");
                    if(parameterdata['error']=='ok') {
                        $('#formulariokanan').trigger("reset");
                         document.getElementById("divmensaje").innerHTML="";

                        document.getElementById("divmensaje").innerHTML = "<div class=\"alert alert-success\" role=\"alert\">" +
                                "<strong>Registro Exitoso</strong> ¡Gracias!" +
                                "</div>" +
                                "<br>";
                    }else if(parameterdata['error']=='Ya Existe'){
                        document.getElementById("divmensaje").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">" +
                                "<strong>Ya Existe el Registro con el Correo o Celular</strong>"+
                                "</div>" +
                                "<br>";

                    }else{
                          document.getElementById("divmensaje").innerHTML = "<div class=\"alert alert-danger\" role=\"alert\">" +
                                "<strong>Ocurrio un Error</strong>"+
                                "</div>" +
                                "<br>";

                    }

                    <%doc>document.getElementById("divmensaje").innerHTML+="<img src=\"data:;base64,"+parameterdata['imagenencoded']+"\" style=\"width: 220px; height: 220px\">";
                    </%doc>
                },
                error: function () {
                    alert("error");

                },
                complete: function () {
                }
            });

        });
    });

</script>

<body background="${h.url()}/img/formf.jpg" style="background-repeat: no-repeat; background-position-x: 100%">

<br>
<br>
<br>
<br>
<br>

<div class="row">
<img src="${h.url()}/img/Kanan.png" align="left" style="position: relative;top:-30px;left:15px;width: 190px;height: 80px">
</div>

<div class="row">

<form id="formulariokanan" name="formulariokanan" method="POST">
  <div class="form-row">

      <div class="form-group col-md-4">
      <label for="inputEmail4">Nombre(s)</label>
      <input type="text" class="form-control" id="orion_name" name="orion_name" placeholder="Nombre" maxlength="45" required>
    </div>
      <div class="form-group col-md-4">
      <label for="inputEmail4">Apellido Paterno</label>
      <input type="text" class="form-control" id="orion_appaterno" name="orion_appaterno" placeholder="ApPaterno" maxlength="45" required>
    </div>
            <div class="form-group col-md-4">
      <label for="inputEmail4">Apellido Materno</label>
      <input type="text" class="form-control" id="orion_apmaterno" name="orion_apmaterno" placeholder="ApMaterno" maxlength="45" required>
    </div>


    <div class="form-group col-md-8">
      <label for="inputEmail4">Email</label>
      <input type="email" class="form-control" id="orion_email" name="orion_email" placeholder="Email" maxlength="90" required>
    </div>


      <div class="form-group col-md-6">
      <label for="inputEmail4">Celular</label>
      <input type="number" class="form-control" id="orion_celular" name="orion_celular" placeholder="Celular" maxlength="15" required>
    </div>


      <div class="form-group col-md-8">
    <button type="submit" class="btn btn-primary">Registrar</button>
    </div>


  </div>
</form>

</div>

<div class="row" id="divmensaje">


</div>

</body>