
<%inherit file="local:templates.master"/>

<script>


    function changestatepartido(id) {
        var tochange=id.id;
        $.ajax({
            type: "GET",
            url: "${h.url()}/alta/habilita?id=" + tochange,
            contentType: "application/json; charset=utf-8",
            data: {},
            success: function (parameterdata) {
                //Insert HTML code
              document.getElementById("status"+parameterdata.id).innerText=parameterdata.soy;

            },
            error: function () {

            },
            complete: function () {
            }
        });


      }



    function ingresaresultado() {
        var arregloresultados = [];
        var equipos = [];
        for(var i=0;i<${cantidad};i++){
                var resultadolocal=document.getElementById("L"+i).value;
                var teamlocal=document.getElementById("teamL"+i).innerText;

                var resultadovisita=document.getElementById("V"+i).value;
                var teamvisita=document.getElementById("teamV"+i).innerText;


                arregloresultados.push(resultadolocal);
                arregloresultados.push(resultadovisita);
                equipos.push(teamlocal);
                equipos.push(teamvisita);
        }

        var confirma=confirm("¿Estas seguro de guardar el resultado?");
        if(confirma){
            $.ajax({
                    type: "GET",
                    url: "${h.url()}/alta/elresultadopartido?equipos=" + equipos + "&resultados=" + arregloresultados,
                    contentType: "application/json; charset=utf-8",
                    data: {},
                    success: function (parameterdata) {
                        //Insert HTML code
                        alert("EXITOSO");

                    },
                    error: function () {
                        alert("ERROR");
                    },
                    complete: function () {
                    }
            });
        }
      }


      function acomodapuntosylugar() {
                  $.ajax({
                    type: "GET",
                    url: "${h.url()}/calcula",
                    data: {},
                    success: function (parameterdata) {
                        //Insert HTML code
                        alert("EXITOSO");

                    },
                    error: function () {
                        alert("ERROR");
                    },
                    complete: function () {
                    }
            });

        }

</script>

<%
    i=0
    %>


% for item in allrecords:
    <p> <input type="text" id='L${i}' value="${item.puntoslocal}" size=10> <a id="teamL${i}">${item.nombrelocal} </a>
        ${_('vs')}  <a id="teamV${i}">${item.nombrevisita} </a><input type="text" id='V${i}'  value="${item.puntosvisita}" size=10>
        <button onclick="changestatepartido(this);" id="up-${item.idpartido}">${_('UP')}</button> <button onclick="changestatepartido(this);" id="down-${item.idpartido}">${_('DOWN')}
        </button>
         <div id="status${item.idpartido}">
        %if item.habilitado == 0:
             ${_('DOWN')}
        %elif item.habilitado == 1:
             ${_('UP')}
            %else:
             ${_('NULL')}
        %endif
        </div>

    </p>

    <%
    i=i+1
    %>

% endfor

 <button type="button"  onclick="ingresaresultado();" class="btn btn-primary">${_('SAVE RESULT')}</button>
<button type="button"  onclick="acomodapuntosylugar();" class="btn btn-primary">Calcula Puntos</button>
