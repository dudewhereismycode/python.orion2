from pythonorion.model import DBSession,PromoFiles, CalendarFiles
from tg import expose
import base64
import datetime

class ManualController():

    def __init__(self):
        pass

    @expose('pythonorion.templates.server1.manual')
    def open(self,**kw):
        return dict(page="manual",kw=kw)

    @expose('pythonorion.templates.server1.upload_calenario')
    def uploadpromo(self, **kw):
        return dict(page="upload_calenario", kw=kw)

    @expose('pythonorion.templates.server1.calendario')
    def calendario(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(CalendarFiles).filter_by(server1_id=kw['appid']).all()
        else:
            handler = DBSession.query(CalendarFiles).filter_by(internal_id=2).all()

        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False

        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                  if len(item.url) > 0:
                      kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()

        return dict(page="calendario",kw=kw,image=image,video=kw['video'])

    @expose('json')
    def savepromo(self, **kw):
        image = CalendarFiles()
        error = "ok"
        if kw["image"] == "undefined":
            return dict(error="Archivo obligatorio")

        fileitem = kw["image"]
        if fileitem.file:
            fileName = fileitem.filename
            if fileName.find(".png") > 0 or fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".gif") > 0:
                if image == None:
                    image = CalendarFiles()
                image.file = fileitem.file.read()
                image.type = 1
            else:
                if fileName.find(".mp4") > 0 or fileName.find(".mov") > 0:
                    if image == None:
                        image = CalendarFiles()
                    image.file = fileitem.file.read()
                    image.type = 0
                else:
                    return dict(error="Archivo obligatorio de tipo PNG, JPEG, DOC, PDF, GIF, MP4, MOV")

            image.application_id = kw['application_id']
            image.internal_id = kw['internal_id']
            image.expiration = kw['expiration']
            image.start = kw['start']
            image.url = kw['url']
            DBSession.add(image)
            DBSession.flush()

        return dict(error=error)
