from pythonorion.model import DBSession,PromoFiles, LogPromo

from tg import expose
import base64
import datetime
from calendar import monthrange
from pythonorion.lib.jqgrid import jqgridDataGrabber
import requests
from pythonorion.lib.helpers import urlpluto, urlapi4sun
from tg import render_template
import arrow

class PromosController():

    def __init__(self):
        pass

    @expose('pythonorion.templates.server1.images')
    def images(self, **kw):
        return dict(page='images', application_id=kw['application_id'], internal_id=kw['internal_id'])

    @expose('json')
    def load_image(self, **kw):
        filter = [('internal_id', 'eq', kw['internal_id'])]
        return jqgridDataGrabber(PromoFiles, 'id', filter, kw).loadGrid()

    @expose('json')
    def update_image(self, **kw):
        filter = [('internal_id', 'eq', kw['internal_id'])]
        return jqgridDataGrabber(PromoFiles, 'id', filter, kw).updateGrid()

    @expose('pythonorion.templates.server1.log')
    def promo_view(self, **kw):
        return dict(page='log', application_id=kw['application_id'], internal_id=kw['internal_id'])

    @expose('json')
    def load_log(self, **kw):
        filter = []
        return jqgridDataGrabber(LogPromo, 'id', filter, kw).loadGrid()

    @expose('json')
    def update_log(self, **kw):
        filter = []
        return jqgridDataGrabber(LogPromo, 'id', filter, kw).updateGrid()

    @expose('pythonorion.templates.server1.promos')
    def promo2(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(LogPromo).filter_by(app_id=kw['appid']).first()
            if handler is None:
                handler.last_login = mydate
                handler.counter = handler.counter + 1
            else:
                handler = LogPromo()
                handler.app_id = kw['appid']
                handler.counter = 1
                handler.first_login = mydate
                handler.last_login = mydate
                DBSession.add(handler)
            DBSession.flush()

        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False
        handler = DBSession.query(PromoFiles).filter_by(internal_id=2).all()
        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                    if len(item.url) > 0:
                        kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()
        if 'appid' in kw:
            r = requests.get(urlpluto() + "/orion/renovationGrid?server_id=" + str(kw['appid']), auth=('lusa', '1234'))
            json_object = r.json()
        return dict(page="promos", kw=kw, image=image, video=kw['video'])

    @expose('pythonorion.templates.server1.promos')
    def promo(self,**kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(LogPromo).filter_by(app_id=kw['appid']).first()
            if handler != None:
                handler.last_login = mydate
                handler.counter = handler.counter + 1
            else:
                handler = LogPromo()
                handler.app_id = kw['appid']
                handler.counter = 1
                handler.first_login = mydate
                handler.last_login = mydate
                DBSession.add(handler)
            DBSession.flush()

        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False
        handler = DBSession.query(PromoFiles).filter_by(internal_id=2,notification=False).all()
        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("b'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                    if len(item.url) > 0:
                        kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()

        # if 'appid' in kw:
        #     r = requests.get(urlpluto()+"/orion/renovationGrid?server_id="+str(kw['appid']),auth=('lusa', '1234'))
        #     rows = r.json()
        #     print(rows)
        #     if rows != []:
        #         print("NO ESTA VACIO")
        return dict(page="promos",kw=kw,image=image,video=kw['video'])

    @expose('json')
    def loadRenovationsGrid(self, **kw):
        rows = {}
        rows['rows'] = []

        if 'appid' in kw:
            handler = DBSession.query(LogPromo).filter_by(app_id=kw['appid']).first()
            print("handler",handler)
            if handler is not None:
                if handler.activated == 0:
                    url  = urlpluto() + "/orion/renovationGrid?server_id=" + str(kw['appid'])
                    r = requests.get(url, auth=('lusa', '1234'))
                    rows = r.json()

        return dict(rows=rows['rows'])

    @expose('pythonorion.templates.server1.upload_promos')
    def uploadpromo(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%Y-%m-%dT%H:%M:00')
        kw['now'] = mydate
        return dict(page="upload_promos",kw=kw)

    @expose('json')
    def savepromo(self, **kw):
        print("SAVE PROMO")
        print(kw)
        image = DBSession.query(PromoFiles).filter_by(internal_id = kw['internal_id']).all()
        for item in image:
            start = kw['start'].replace("T"," ")
            start = start+':00'
            expiration = kw['expiration'].replace("T", " ")
            expiration = expiration + ':00'
            if str(item.start) == str(start) and str(item.expiration) == str(expiration):
                DBSession.delete(item)
                DBSession.flush()

        image = PromoFiles()
        error = "ok"
        if kw["start"] == "":
            return dict(error="Fecha y Hora de inicio obligatoria")
        if kw["expiration"] == "":
            return dict(error="Fecha y Hora de expiración obligatoria")
        if kw["image"] == "undefined":
            return dict(error="Archivo obligatorio")

        fileitem = kw["image"]
        if fileitem.file:
            fileName = fileitem.filename
            if fileName.find(".png") > 0 or fileName.find(".jpeg") > 0 or fileName.find(".jpg") > 0 or fileName.find(".gif") > 0:
                if image == None:
                    image = PromoFiles()
                image.file = fileitem.file.read()
                image.type = 1
            else:
                if fileName.find(".mp4") > 0 or fileName.find(".mov") > 0:
                    if image == None:
                        image = PromoFiles()
                    image.file = fileitem.file.read()
                    image.type = 0
                else:
                    return dict(error="Archivo obligatorio de tipo PNG, JPEG, DOC, PDF, GIF, MP4, MOV")

            image.application_id = kw['application_id']
            image.internal_id = kw['internal_id']
            image.expiration = kw['expiration']
            image.start = kw['start']
            image.url = kw['url']

            if kw['notification'] == "true":
                image.notification = True
                image.noti_message = kw['noti_message']
                image.noti_title = kw['noti_title']
            else:
                image.notification = False
                image.noti_title = ""
                image.noti_message = ""

            DBSession.add(image)
            DBSession.flush()

        return dict(error=error)

    @expose('pythonorion.templates.server1.open_justpromoapp')
    def showjustpromo(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(PromoFiles).filter_by(server1_id=kw['appid'],notification=True).first()
        else:
            handler = DBSession.query(PromoFiles).filter_by(internal_id=2,notification=True).all()

        kw['appid']=12
        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False
        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                  if len(item.url) > 0:
                    kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()

        return dict(page="promos",kw=kw,image=image,video=kw['video'])


    @expose('pythonorion.templates.server1.open_promoapp')
    def showpromo(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(PromoFiles).filter_by(server1_id=kw['appid'],notification=True).first()
        else:
            handler = DBSession.query(PromoFiles).filter_by(internal_id=2,notification=True).all()

        kw['appid']=12
        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False
        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                  if len(item.url) > 0:
                    kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()

        return dict(page="promos",kw=kw,image=image,video=kw['video'])


    @expose('pythonorion.templates.server1.open_promos')
    def showpromo2(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        if 'appid' in kw:
            handler = DBSession.query(PromoFiles).filter_by(server1_id=kw['appid'],notification=False).first()
        else:
            handler = DBSession.query(PromoFiles).filter_by(internal_id=2,notification=False).all()

        kw['appid']=12
        image = False
        kw['img'] = ""
        kw['url'] = ""
        kw['video'] = False
        for item in handler:
            if item.start <= mydate and item.expiration >= mydate:
                image = True
                kw['img'] = base64.b64encode(item.file)
                kw['img'] = str(kw['img']).replace("b'", "")
                kw['img'] = str(kw['img']).replace("'", "")
                if item.type == 0:
                    kw['video'] = True
                if item.url != None:
                  if len(item.url) > 0:
                    kw['url'] = "href=" + item.url
            else:
                if item.expiration <= mydate:
                    DBSession.delete(item)
                    DBSession.flush()

        print("end showpromo")
        return dict(page="promos",kw=kw,image=image,video=kw['video'])


    @expose('pythonorion.templates.server1.calendar')
    def calendary(self, **kw):
        mydate = datetime.datetime.utcnow()
        utc = arrow.get(mydate)
        mydate = utc.to('America/Mexico_City').strftime('%d-%m-%Y %H:%M:%S')
        mydate = datetime.datetime.strptime(mydate, '%d-%m-%Y %H:%M:%S')

        date = mydate
        date_str = str(date.month) + '-01-' + str(date.year)
        first_day = datetime.datetime.strptime(date_str,'%m-%d-%Y').date()
        range = monthrange(date.year,date.month)
        count = -int(first_day.isoweekday())+1
        html = []
        while count < int(range[1])+1:
            count += 1
            if count > 0:
                if count == int(date.day):
                    html.append({'data': str(count),'class':'active'})
                else:
                    html.append({'data': str(count), 'class': ''})
            else:
                html.append({'data': '', 'class': ''})

        handler = DBSession.query(PromoFiles).filter_by(internal_id=kw['internal_id']).all()
        for item in handler:
            img11 = base64.b64encode(item.file)
            img11 = str(img11).replace("b'", "")
            img11 = str(img11).replace("'", "")
            video = False
            url = ""
            if item.type == 0:
                video = True
            if item.url != None:
                if len(item.url) > 0:
                    url = "href=" + item.url
            kw[str(item.start.day)] = {'date': item.start.day, 'img': img11,'video':video,'url':url}
            start = int(item.start.day)
            stop = int(item.expiration.day)
            while start < stop+1:
                kw[str(start)] = {'date': item.expiration.day, 'img': img11, 'video': video, 'url': url}
                start+=1
        kw['html'] = html
        kw['month'] = date.strftime("%B")
        kw['year'] = date.strftime("%Y")
        return dict(page="calendar",kw=kw)

    @expose('pythonorion.templates.server1.link')
    def links(self, **kw):
        return dict(page="link")

    @expose('pythonorion.templates.server1.link')
    def showlinks(self, **kw):
        return dict(page="link")

    @expose('pythonorion.templates.server1.chekdo_video')
    def chekdov(self, **kw):
        return dict(page="chekdo_video")

    @expose('json')
    def testnoti(self,**kw):
        dialogtemplate = render_template(kw, "mako", 'pythonorion.templates.server1.test_nofity')
        return dict(dedwizardtemplate=dialogtemplate, error="ok")

    @expose('json')
    def sendNoti(self,**kw):
        error = "ok"
        handler = DBSession.query(PromoFiles).filter_by(id=kw['id']).first()
        if handler is not None:
            params = ""
            params += "?keycode=2b4891ce"
            params += "&telephone="+kw['phone']
            if kw['android'] == "true":
                params2 = '&json_data={"command":"promo","msg":"' + handler.noti_title + '","url":"' + handler.noti_message + '"}'
                url = urlapi4sun()+"mobile/notifications/android"+params+params2
                request = requests.post(url)
            if kw['iOS'] == "true":
                params2 = '&json_data={"news":"0","alert":"'+ handler.noti_title+'","link_url":"'+handler.noti_message+'"}'
                params2 += "&production=1"
                params2 += "&content-available=0"
                url = urlapi4sun() + "mobile/notifications/ios"+params+params2
                request = requests.post(url)
        else:
            error = "La promoción seleccionada sin notificación configurada"
        return dict(error=error)

    @expose('json')
    def refresh(self,**kw):
        handler = DBSession.query(LogPromo).all()
        for item in handler:
            item.counter = 0
        DBSession.flush()
        return dict(error='ok')



