# -*- coding: utf-8 -*-
"""Main Controller"""

from tg import expose, flash, require, url, lurl
from tg import request, redirect, tmpl_context
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.exceptions import HTTPFound
from tg import predicates
from pythonorion import model
from pythonorion.controllers.secure import SecureController
from pythonorion.model import DBSession
from tgext.admin.tgadminconfig import BootstrapTGAdminConfig as TGAdminConfig
from tgext.admin.controller import AdminController
from pythonorion.controllers.partidos import AboutPartidos
from tg import predicates
from pythonorion.model.tables import Partido,Pronostico,UsuariosParticipantes,usuario_pronostico_table,pronostico_partido_table,Finalposicion
from pythonorion.model.tables import Kananregistro
import qrcode
import qrcode.image.svg
from io import BytesIO
from tg import app_globals
import os
import base64
from tgext.mailer.mailer import Mailer
from tgext.mailer import Message
from pythonorion.lib.helpers import url
from pythonorion.lib.utility import ExportPDF
import random
from wand.image import Image as wi
from pdf2image import convert_from_path
from sqlalchemy import or_



from pythonorion.lib.base import BaseController
from pythonorion.controllers.error import ErrorController

from pythonorion.lib.services import ServicesController
from pythonorion.controllers.server1.promos import PromosController
from pythonorion.controllers.server1.manual import ManualController

import os
import sendgrid
from sendgrid.helpers.mail import *

__all__ = ['RootController']


class RootController(BaseController):
    """
    The root controller for the python.orion application.

    All the other controllers and WSGI applications should be mounted on this
    controller. For example::

        panel = ControlPanelController()
        another_app = AnotherWSGIApplication()

    Keep in mind that WSGI applications shouldn't be mounted directly: They
    must be wrapped around with :class:`tg.controllers.WSGIAppController`.

    """
    secc = SecureController()
    admin = AdminController(model, DBSession, config_type=TGAdminConfig)

    error = ErrorController()

    services = ServicesController()


    alta=AboutPartidos()
    promos = PromosController()
    manual = ManualController()

    #allow_only = predicates.not_anonymous()

    def _before(self, *args, **kw):
        tmpl_context.project_name = "pythonorion"

    @expose('pythonorion.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('pythonorion.templates.index')
    def index(self):
        """Handle the front-page."""
        return dict(page='index')

    @expose('pythonorion.templates.formulariokanan')
    def kanan(self):
        """Handle the front-page."""
        return dict(page='index')


    @expose('pythonorion.templates.pruebacorreo')
    def imagenboleto54321(self):
        """Handle the front-page."""
        queryqrkanan=DBSession.query(Kananregistro).filter_by(id_kananregistro=1).first()
        imagenencoded = queryqrkanan.qr.decode("utf-8")
        folio=queryqrkanan.id_kananregistro

        return dict(page='index',imagenencoded=imagenencoded,folio=folio)


    @expose('pythonorion.templates.todoqr')
    def kananqrs(self):
        """Handle the front-page."""

        list=[]
        app_dir = os.getenv('ORION_DIR')
        if app_dir is None:
            app_dir = os.getcwd()
        ruta = app_dir + os.sep
        destino = ruta + "pythonorion/public/img/codigosqr/"
        try:
            os.mkdir(destino)
        except:
            pass
        #img.save(destino + "qr_" + str(querynuevoregistro.id_kananregistro) + ".jpg")
        queryregistros=DBSession.query(Kananregistro).all()
        for item in queryregistros:
            laimagen = destino + "qr_" + str(item.id_kananregistro) + ".jpg"
            with open(laimagen, "rb") as image_file:
                encoded_stringqr = base64.b64encode(image_file.read())
            list.append(encoded_stringqr.decode("utf-8"))

        return dict(page='index',list=list)


    @expose('json')
    def registrodekanandatos(self,**kw):
        #print(kw)
        #{'orion_apmaterno': 'Lopez', 'orion_name': 'Diego Essai', 'orion_email': 'ddff@dr.com', 'orion_modelomoto': '8ukjujk', 'orion_appaterno': 'Santillan',
        # 'orion_celular': '227728', 'orion_telefono': '2588228'}
        querynuevoregistrohay=DBSession.query(Kananregistro).filter_by(nombre=kw['orion_name']).filter_by(appaterno=kw['orion_appaterno']).filter_by(apmaterno=kw['orion_apmaterno'])\
                            .filter_by(email=kw['orion_email']).filter_by(celular=kw['orion_celular'])\
                            .first()

        querynuevoregistrohay=DBSession.query(Kananregistro).filter(or_(Kananregistro.email==kw['orion_email'],Kananregistro.celular==kw['orion_celular'])).first()


        if querynuevoregistrohay is None:
            newregistro=Kananregistro()
            newregistro.nombre=kw['orion_name']
            newregistro.appaterno=kw['orion_appaterno']
            newregistro.apmaterno=kw['orion_apmaterno']
            newregistro.email=kw['orion_email']
            newregistro.celular=kw['orion_celular']
            DBSession.add(newregistro)
            DBSession.flush()

            querynuevoregistro=DBSession.query(Kananregistro).filter_by(nombre=kw['orion_name']).filter_by(appaterno=kw['orion_appaterno']).filter_by(apmaterno=kw['orion_apmaterno'])\
                                .filter_by(email=kw['orion_email']).filter_by(celular=kw['orion_celular'])\
                                .first()

            size = 40
            msg="Folio: "+str(querynuevoregistro.id_kananregistro)+"\n"+\
                "Nombre: "+str(querynuevoregistro.nombre)+" "+str(querynuevoregistro.appaterno)+" "+str(querynuevoregistro.apmaterno)+" \n"+\
                "Correo: "+str(querynuevoregistro.email)+"\n"+\
                "Celular: "+str(querynuevoregistro.celular)




            qr = qrcode.QRCode(box_size=40)
            qr.add_data(msg)
            # qr.make(fit=True)
            img = qr.make_image()
            app_dir = os.getenv('ORION_DIR')
            if app_dir is None:
                app_dir = os.getcwd()
            ruta = app_dir + os.sep
            destino = ruta + "pythonorion/public/img/codigosqr/"
            try:
                os.mkdir(destino)
            except:
                pass
            img.save(destino + "qr_" + str(querynuevoregistro.id_kananregistro) + ".jpg")
            laimagen = destino + "qr_" + str(querynuevoregistro.id_kananregistro) + ".jpg"
            with open(laimagen, "rb") as image_file:
                encoded_stringqr = base64.b64encode(image_file.read())
                querynuevoregistro.qr=encoded_stringqr

            imagenencoded=encoded_stringqr.decode("utf-8")


            recipients = []
            recipients.append(kw['orion_email'])
            host="smtp.sendgrid.net"
            port=587
            username="tracker_mexico"
            password="Tracker01"
            subject = "Registro Evento 1927 WORKSHOP"
            sender="ventas@kanangps.mx"
            html=""

            destinoboleto = ruta + "pythonorion/public/img/Boleto_acceso.png"
            with open(destinoboleto, "rb") as image_file:
                imagenboletobase64 = base64.b64encode(image_file.read())


            kw['folio']=querynuevoregistro.id_kananregistro
            kw['elboleto']=imagenboletobase64.decode("utf-8")
            kw['imagenencoded']=encoded_stringqr.decode("utf-8")

            i=0
            cadena=''
            while i<8:
                char = "abcdefghijklmnopqrstuvwyz"
                letra_aleatoria = random.choice(char)
                cadena=cadena+letra_aleatoria
                i+=1

            file_name = ExportPDF.create(kw, 'Boleto_'+cadena+str(querynuevoregistro.id_kananregistro), 'pythonorion.templates.correoevento',
                                         'prueba')

            destinopdfconv = ruta + "pythonorion/public"+file_name

            pages = convert_from_path(destinopdfconv)

            for page in pages:
                page.save(ruta + "pythonorion/public/"+cadena+str(querynuevoregistro.id_kananregistro)+'.jpg', 'JPEG')


            laimagendeboleto=ruta + "pythonorion/public/"+cadena+str(querynuevoregistro.id_kananregistro)+'.jpg'
            with open(laimagendeboleto, "rb") as image_file:
                imagenfinal = base64.b64encode(image_file.read())

            html += "<style type=\"text/css\">" \
                    "#chistes{position: relative;}" \
                    ".sobre {position:absolute;top:80px;left:70px;border:none;heigth: 20%;width:20%;}" \
                    ".texto-encima{position: absolute;top: 10px;left: 240px;color: red;}" \
                    "</style>"

            html += "<div id=\"chistes\">"
            html += "<img style=\"width:20%; height: 20%;\" src=\""+url()+"/img/Kanan.png\" alt=\"KANANLOGO\">"
            html +="<h3>¡Gracias por su Registro!</h3> <i>Presenta el siguiente boleto para poder ingresar al evento </i> <h3>¡Los Esperamos No Falten!</h3>"
            html += "<img style=\"width:90%; height: 90%;\" src=\""+url()+"/"+cadena+str(querynuevoregistro.id_kananregistro)+".jpg\" alt=\"KANANBOLETOFINAL\">"

            try:
                # mailer = Mailer(host=host, port=port, tls=True, username=username, password=password, debug=0)
                # message = Message(
                #             subject=subject,
                #             sender="Kanan <"++">",
                #             recipients=recipients,
                #             html=html,
                #         )
                # mailer.send_immediately(message, fail_silently=False)

                sg = sendgrid.SendGridAPIClient(api_key=password)
                # sg = sendgrid.SendGridAPIClient(api_key=os.environ.get('SENDGRID_API_KEY'))
                from_email = Email(sender)
                to_email = To(recipients)
                subject = subject
                content = HtmlContent(html)

                mail = Mail(from_email, to_email, subject, content)
                response = sg.client.mail.send.post(request_body=mail.get())

            except:
                return dict(error='error_mail')


            return dict(error='ok')

        else:
            return dict(error='Ya Existe')


    @expose('pythonorion.templates.about')
    def about(self):
        """Handle the 'about' page."""
        return dict(page='about')



    @expose('pythonorion.templates.environ')
    def environ(self):
        """This method showcases TG's access to the wsgi environment."""
        return dict(page='environ', environment=request.environ)

    @expose('pythonorion.templates.data')
    @expose('json')
    def data(self, **kw):
        """
        This method showcases how you can use the same controller
        for a data page and a display page.
        """
        return dict(page='data', params=kw)



    @expose('pythonorion.templates.index')
    @require(predicates.has_permission('manage', msg=l_('Only for managers')))
    def manage_permission_only(self, **kw):
        """Illustrate how a page for managers only works."""
        return dict(page='managers stuff')

    @expose('pythonorion.templates.index')
    @require(predicates.is_user('editor', msg=l_('Only for the editor')))
    def editor_user_only(self, **kw):
        """Illustrate how a page exclusive for the editor works."""
        return dict(page='editor stuff')
    #
    # @expose('pythonorion.templates.login')
    # def login(self, came_from=lurl('/'), failure=None, login=''):
    #     """Start the user login."""
    #     if failure is not None:
    #         if failure == 'user-not-found':
    #             flash(_('User not found'), 'error')
    #         elif failure == 'invalid-password':
    #             flash(_('Invalid Password'), 'error')
    #
    #     login_counter = request.environ.get('repoze.who.logins', 0)
    #     if failure is None and login_counter > 0:
    #         flash(_('Wrong credentials'), 'warning')
    #
    #     return dict(page='login', login_counter=str(login_counter),
    #                 came_from=came_from, login=login)

    @expose()
    def check(self):
        return "UP"

    @expose('json')
    def calcula(self):
        tablaposicion=[]

        queryusuarios = DBSession.query(UsuariosParticipantes).all()
        if queryusuarios is not None:
            for item2 in queryusuarios:
                tablapuntos = []

                newqueryusu_usupro = DBSession.query(usuario_pronostico_table).filter_by(idusuario=item2.idusuario).all()
                lugarprediccion = item2.lugarmexico
                item2.puntosoctavos = 0
                item2.puntoscuartos = 0
                item2.puntossemifinal = 0
                item2.puntostercerlugar = 0
                item2.puntosfinal = 0
                item2.puntos = 0
                puntos = 0
                for item3 in newqueryusu_usupro:
                    newqueryusupro_pro = DBSession.query(Pronostico).filter_by(idpronostico=item3.idpronostico).first()
                    prediccionlocal=newqueryusupro_pro.puntoslocal
                    prediccionvisita = newqueryusupro_pro.puntosvisita
                    prediccionestado=newqueryusupro_pro.prediccionpartido
                    # print("LOCAL: ",prediccionlocal)
                    # print("VISITA: ",prediccionvisita)
                    # print("y quedo:")


                    newquerypro_propa = DBSession.query(pronostico_partido_table).filter_by(idpronostico=newqueryusupro_pro.idpronostico).all()
                        #
                    for item4 in newquerypro_propa:
                        newquerypropa_pa = DBSession.query(Partido).filter_by(idpartido=item4.idpartido).first()
                        if(newquerypropa_pa.puntoslocal is not None and newquerypropa_pa.puntosvisita is not None):
                            definitivolocal=newquerypropa_pa.puntoslocal
                            definitivovisita=newquerypropa_pa.puntosvisita
                            definitivoestado=newquerypropa_pa.estadopartido

                            if(definitivolocal>definitivovisita):
                                definitivodiferencia=definitivolocal - definitivovisita
                            if(definitivolocal<definitivovisita):
                                definitivodiferencia=definitivovisita - definitivolocal
                            if(definitivolocal==definitivovisita):
                                definitivodiferencia=0

                            if(prediccionlocal>prediccionvisita):
                                predicciondiferencia=prediccionlocal - prediccionvisita
                            if(prediccionlocal<prediccionvisita):
                                predicciondiferencia=prediccionvisita - prediccionlocal
                            if(prediccionlocal==prediccionvisita):
                                predicciondiferencia=0

                            # print("local: ", newquerypropa_pa.puntoslocal)
                            # print("visita: ", newquerypropa_pa.puntosvisita)
                            auxpuntos=0
                            if(prediccionlocal == definitivolocal and prediccionvisita == definitivovisita):
                                puntos=puntos+2
                                auxpuntos=auxpuntos+2


                            if(definitivodiferencia == predicciondiferencia and definitivoestado==prediccionestado):
                                puntos = puntos + 1
                                auxpuntos = auxpuntos + 1


                            if(definitivovisita==prediccionvisita):
                                puntos = puntos + 1
                                auxpuntos = auxpuntos + 1


                            if(definitivolocal==prediccionlocal):
                                puntos = puntos + 1
                                auxpuntos = auxpuntos + 1

                            if(prediccionestado==definitivoestado):
                                puntos=puntos + 1
                                auxpuntos = auxpuntos +1

                            if(newquerypropa_pa.fase=='OCTAVOS'):
                                item2.puntosoctavos = puntos
                            if (newquerypropa_pa.fase == 'CUARTOS'):
                                item2.puntoscuartos = puntos - item2.puntosoctavos
                            if (newquerypropa_pa.fase == 'SEMIFINAL'):
                                item2.puntossemifinal = puntos - item2.puntosoctavos -item2.puntoscuartos
                            if (newquerypropa_pa.fase == 'TERCERLUGAR'):
                                item2.puntostercerlugar = puntos - item2.puntosoctavos - item2.puntoscuartos -item2.puntossemifinal
                            if (newquerypropa_pa.fase == 'FINAL'):
                                item2.puntosfinal = puntos - item2.puntosoctavos - item2.puntoscuartos -item2.puntossemifinal - item2.puntostercerlugar

                            #tablapuntos.append({newquerypropa_pa.alias: auxpuntos})
                            newqueryusupro_pro.puntosconseguidos=auxpuntos

                if item2.nombre is None:
                    tablaposicion.append({'username': item2.username, 'puntos': tablapuntos, 'total': puntos,'name': item2.nombre,'lugarmexico': item2.lugarmexico})

                else:
                    thename = item2.nombre.split(" ")
                    tablaposicion.append({'username': item2.username, 'puntos': tablapuntos,'total': puntos,'name': thename[0],'lugarmexico': item2.lugarmexico})
                #tablaposicion.append({'puntos,'+item2.username: puntos})
                #print(tablaposicion)


                puntos2 = item2.puntosoctavos + item2.puntoscuartos + item2.puntossemifinal + item2.puntostercerlugar + item2.puntosfinal
                item2.puntos=puntos2
                DBSession.flush()
            queryusuarios_paraposicion = DBSession.query(UsuariosParticipantes).all()
            lista=[]
            for item in queryusuarios_paraposicion:
                lista.append({"user": item.username ,"puntos": item.puntos})


            my_list = sorted(lista, key=lambda k: k['puntos'], reverse=True)
            medida=len(my_list)
            lugar=1
            contador=1
            while contador < medida:
                if(contador-1 == 0):
                    query=DBSession.query(UsuariosParticipantes).filter_by(username=my_list[contador-1]['user']).first()
                    query.posicion=lugar
                    DBSession.flush()
                    lugar=lugar+1

                if(my_list[contador]['puntos']==my_list[contador-1]['puntos']):
                    lugar=lugar - 1
                query = DBSession.query(UsuariosParticipantes).filter_by(username=my_list[contador]['user']).first()
                query.posicion = lugar
                DBSession.flush()
                lugar=lugar+1
                contador=contador+1

                allusers = DBSession.query(UsuariosParticipantes).order_by(UsuariosParticipantes.posicion).all()



        return dict(tablaposicion=tablaposicion)


    #
    # @expose('pythonorion.templates.tabla')
    # def tablaposiciones(self):
    #     tabla=self.calcula()
    #     allpartidos=DBSession.query(Partido).all()
    #     #print(tabla['tablaposicion'])
    #     listaposicion=[]
    #     my_list = sorted(tabla['tablaposicion'], key=lambda k: k['total'], reverse=True)
    #
    #     allusers = DBSession.query(UsuariosParticipantes).order_by(UsuariosParticipantes.posicion).all()
    #     cuantosuno=0
    #     cuantosdos=0
    #     cuantostres=0
    #     for each in allusers:
    #         if(each.posicion==1):
    #             cuantosuno=cuantosuno+1
    #         if(each.posicion == 2):
    #             cuantosdos = cuantosdos + 1
    #         if (each.posicion == 3):
    #             cuantostres = cuantostres + 1
    #
    #
    #     print("HAY EN 1",cuantosuno)
    #     print("HAY EN 2",cuantosdos)
    #     print("HAY EN 3",cuantostres)
    #
    #     querytercer = DBSession.query(UsuariosParticipantes).filter_by(posicion=3).all()
    #     for item in querytercer:
    #         if(item.lugarmexico!=2):
    #             item.posicion=item.posicion+1
    #             DBSession.flush()
    #
    #     queryprimer=DBSession.query(UsuariosParticipantes).filter_by(posicion=1).all()
    #     querysegundo = DBSession.query(UsuariosParticipantes).filter_by(posicion=2).all()
    #     querytercer = DBSession.query(UsuariosParticipantes).filter_by(posicion=3).all()
    #
    #     allusers = DBSession.query(UsuariosParticipantes).order_by(UsuariosParticipantes.posicion).all()
    #     return dict(tabla=my_list,allpartidos=allpartidos,allusers=allusers,primer=queryprimer,segundo=querysegundo,tercer=querytercer)

    # @expose()
    # def post_login(self, came_from=lurl('/')):
    #     """
    #     Redirect the user to the initially requested page on successful
    #     authentication or redirect her back to the login page if login failed.
    #
    #     """
    #     if not request.identity:
    #         login_counter = request.environ.get('repoze.who.logins', 0) + 1
    #         redirect('/login',
    #                  params=dict(came_from=came_from, __logins=login_counter))
    #     userid = request.identity['repoze.who.userid']
    #     flash(_('Welcome back, %s!') % userid)
    #
    #     # Do not use tg.redirect with tg.url as it will add the mountpoint
    #     # of the application twice.
    #     return HTTPFound(location=came_from)
    #
    # @expose()
    # def post_logout(self, came_from=lurl('/')):
    #     """
    #     Redirect the user to the initially requested page on logout and say
    #     goodbye as well.
    #
    #     """
    #     flash(_('We hope to see you soon!'))
    #     return HTTPFound(location=came_from)


