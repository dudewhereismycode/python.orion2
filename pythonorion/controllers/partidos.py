import json
from tg import app_globals
import json
from pythonorion.model import DBSession
from pythonorion.model.auth import User
from pythonorion.model.tables import Partido,Pronostico,UsuariosParticipantes,usuario_pronostico_table,pronostico_partido_table
import requests
import time
from tg.i18n import ugettext as _
from tg import expose,require
from tg import predicates
import cgi
from tg import predicates



class AboutPartidos():
    def __init__(self):
        pass

    @expose('pythonorion.templates.altapartido')
    @require(predicates.not_anonymous())
    def partido(self):
        return dict(page='partido')

    @expose('json')
    def newpartido(self,**kw):
        team1=kw['team1']
        team2 = kw['team2']
        date = kw['date']
        hour=kw['hour']
        fase=kw['fase']
        newitem=Partido()
        newitem.nombrelocal=team1
        newitem.nombrevisita=team2
        newitem.fecha=date+' '+hour
        newitem.alias=team1.replace(' ', '')+"vs"+team2.replace(' ', '')
        newitem.fase=fase
        DBSession.add(newitem)
        DBSession.flush()
        return dict(error='ok')


    @expose('pythonorion.templates.altaresultadopartido')
    @require(predicates.not_anonymous())
    def resultadopartido(self):
        allrecords=DBSession.query(Partido).all()

        return dict(page='resultadopartido',allrecords=allrecords,cantidad=len(allrecords))

    @expose('json')
    def elresultadopartido(self,**kw):
        equipos=kw['equipos']
        resultados=kw['resultados']
        equiposplit=equipos.split(",")
        resultadossplit=resultados.split(",")
        i=0

        while i < len(equiposplit):
            query=DBSession.query(Partido).filter_by(nombrelocal=equiposplit[i]).filter_by(nombrevisita=equiposplit[i+1]).first()
            if query is not None:
                if(resultadossplit[i] == '' or resultadossplit[i] == ' '):
                    query.puntoslocal = None
                    query.puntosvisita =  None
                else:
                    query.puntoslocal=int(resultadossplit[i])
                    query.puntosvisita=int(resultadossplit[i+1])
                    if(query.puntoslocal>query.puntosvisita):
                        query.estadopartido='LOCAL'
                    if (query.puntoslocal < query.puntosvisita):
                        query.estadopartido = 'VISITA'
                    if (query.puntoslocal == query.puntosvisita):
                        query.estadopartido = 'EMPATE'
            DBSession.flush()

            i=i+2

        return dict(error="OK")


    @expose('json')
    def habilita(self,**kw):
        id=kw['id']
        arrayto=id.split("-")

        if(arrayto[0]=="up"):
            query=DBSession.query(Partido).filter_by(idpartido=arrayto[1]).first()
            if query is not None:
                query.habilitado=1
                DBSession.flush()
            return dict(soy="UP",id=arrayto[1])

        if(arrayto[0]=="down"):
            query=DBSession.query(Partido).filter_by(idpartido=arrayto[1]).first()
            if query is not None:
                query.habilitado=0
                DBSession.flush()

            return dict(soy="DOWN",id=arrayto[1])

        return dict(error="paso")


    @expose('pythonorion.templates.altapronostico')
    @require(predicates.not_anonymous())
    def pronostico(self,**kw):
        fecha=time.strftime("%d/%m/%y")
        fechaseparada=fecha.split("/")
        if int(fechaseparada[0]) < 30 :
            if int(fechaseparada[1]) == 6:
                estado='bien'
            else:
                estado='yano'
        else:
            estado='yano'
        user=kw['user']
        listlocal=[]
        listvisita=[]
        listalias=[]
        lugarprediccion=0
        puntos=0
        posicion=0
        query=DBSession.query(UsuariosParticipantes).filter_by(username=user).first()
        if query is not None and query.ingresa==1:
            newqueryusu_usupro=DBSession.query(usuario_pronostico_table).filter_by(idusuario=query.idusuario).all()
            #lugarprediccion=query.lugarmexico
            posicion=query.posicion
            puntos=query.puntosoctavos + query.puntoscuartos + query.puntossemifinal + query.puntostercerlugar + query.puntosfinal

            for item in newqueryusu_usupro:
                newqueryusupro_pro=DBSession.query(Pronostico).filter_by(idpronostico=item.idpronostico).first()

                listlocal.append(newqueryusupro_pro.puntoslocal)
                listvisita.append(newqueryusupro_pro.puntosvisita)

                newquerypro_propa = DBSession.query(pronostico_partido_table).filter_by(idpronostico=newqueryusupro_pro.idpronostico).all()

                for item in newquerypro_propa:
                    newquerypropa_pa = DBSession.query(Partido).filter_by(idpartido=item.idpartido).first()
                    listalias.append(newquerypropa_pa.alias)

            poder=1
        else:

            poder=0
        allrecords = DBSession.query(Partido).all()
        contador=0
        for item in allrecords:
            if item.habilitado==1:
                contador=contador+1


        return dict(page='pronostico',user=user,allrecords=allrecords,cantidad=contador,poder=poder,listlocal=listlocal,listvisita=listvisita,listalias=listalias,
                    lugarprediccion=lugarprediccion,puntos=puntos,estado=estado,posicion=posicion)


    @expose('json')
    def pronosticopartido(self,**kw):
        user=kw['user']
        equipos=kw['equipos']
        resultados=kw['resultados']

        nombre=kw['nombre']
        telefono=kw['telefono']
        equiposplit = equipos.split(",")
        resultadossplit=resultados.split(",")
        i = 0

        query=DBSession.query(UsuariosParticipantes).filter_by(username=user).first()

        if query is None:
            newuser=UsuariosParticipantes()
            newuser.username=user
            newuser.ingresa=1
            newuser.puntos=0
            newuser.nombre=nombre
            newuser.telefono=telefono
            newuser.posicion=0
            DBSession.add(newuser)
            DBSession.flush()


            while i < len(equiposplit):
                newitem = Pronostico()
                newitem.puntoslocal = resultadossplit[i]
                newitem.puntosvisita = resultadossplit[i + 1]
                newitem.nombrepronostico = user+equiposplit[i].replace(' ', '')+"vs"+equiposplit[i+1].replace(' ', '')

                if (newitem.puntoslocal > newitem.puntosvisita):
                    newitem.prediccionpartido = 'LOCAL'
                if (newitem.puntoslocal < newitem.puntosvisita):
                    newitem.prediccionpartido = 'VISITA'
                if (newitem.puntoslocal == newitem.puntosvisita):
                    newitem.prediccionpartido = 'EMPATE'

                DBSession.add(newitem)
                DBSession.flush()


                partido_query=DBSession.query(Partido).filter_by(alias=equiposplit[i].replace(' ', '')+"vs"+equiposplit[i+1].replace(' ', '')).first()
                query_pronostico=DBSession.query(Pronostico).filter_by(nombrepronostico=user+equiposplit[i].replace(' ', '')+"vs"+equiposplit[i+1].replace(' ', '')).first()

                partido_query.prono.append(query_pronostico)

                DBSession.flush()

                query_pronostico = DBSession.query(Pronostico).filter_by(nombrepronostico=user+equiposplit[i].replace(' ', '')+"vs"+equiposplit[i + 1].replace(' ', '')).first()
                user_query = DBSession.query(UsuariosParticipantes).filter_by(username=user).first()

                query_pronostico.participantes.append(user_query)

                DBSession.flush()

                i = i + 2

        else:

            if query.ingresa == 1:
                return dict(error="YA TIENES UN PRONOSTICO INGRESADO NO PUEDES MODIFICAR")
            else:
                query.ingresa = 1
                DBSession.flush()
                while i < len(equiposplit):
                    newitem = Pronostico()
                    newitem.puntoslocal = resultadossplit[i]
                    newitem.puntosvisita = resultadossplit[i + 1]
                    newitem.nombrepronostico = user + equiposplit[i].replace(' ', '') + "vs" + equiposplit[i + 1].replace(' ', '')

                    if (newitem.puntoslocal > newitem.puntosvisita):
                        newitem.prediccionpartido = 'LOCAL'
                    if (newitem.puntoslocal < newitem.puntosvisita):
                        newitem.prediccionpartido = 'VISITA'
                    if (newitem.puntoslocal == newitem.puntosvisita):
                        newitem.prediccionpartido = 'EMPATE'

                    DBSession.add(newitem)
                    DBSession.flush()

                    partido_query = DBSession.query(Partido).filter_by(alias=equiposplit[i].replace(' ', '') + "vs" + equiposplit[i + 1].replace(' ', '')).first()
                    query_pronostico = DBSession.query(Pronostico).filter_by(nombrepronostico=user + equiposplit[i].replace(' ', '') + "vs" + equiposplit[i + 1].replace(' ','')).first()

                    partido_query.prono.append(query_pronostico)

                    DBSession.flush()

                    query_pronostico = DBSession.query(Pronostico).filter_by(nombrepronostico=user + equiposplit[i].replace(' ', '') + "vs" + equiposplit[i + 1].replace(' ','')).first()
                    user_query = DBSession.query(UsuariosParticipantes).filter_by(username=user).first()


                    query_pronostico.participantes.append(user_query)

                    DBSession.flush()

                    i = i + 2

        return dict(error='OK')



