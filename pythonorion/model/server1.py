from sqlalchemy import Date, LargeBinary, DateTime

from pythonorion.model import DeclarativeBase
from sqlalchemy import Column
from sqlalchemy.types import Integer, Unicode, Boolean

class PromoFiles(DeclarativeBase):
    __tablename__='promo_files'
    id=Column(Integer,primary_key=True)
    file = Column(LargeBinary(length=(2 ** 32) - 1))
    expiration = Column(DateTime)
    application_id = Column(Integer)
    server1_id = Column(Integer)
    internal_id = Column(Integer)
    url = Column(Unicode(1000))
    type = Column(Integer) #1 -> imagen ##  0 -> video
    start = Column(DateTime)
    notification = Column(Boolean)
    noti_title = Column(Unicode(50))
    noti_message = Column(Unicode(200))
    sended = Column(Boolean,default=False) #si se envio la notificación

class LogPromo(DeclarativeBase):
    __tablename__='log_promo'
    id = Column(Integer,primary_key=True)
    app_id = Column(Integer)
    counter = Column(Integer)
    first_login = Column(DateTime)
    last_login = Column(DateTime)
    activated = Column(Integer, default=0) #0 esta activo 1 no quieren

class CalendarFiles(DeclarativeBase):
    __tablename__='calendar_files'
    id=Column(Integer,primary_key=True)
    file = Column(LargeBinary(length=(2 ** 32) - 1))
    expiration = Column(DateTime)
    application_id = (Integer)
    server1_id = (Integer)
    internal_id = Column(Integer)
    url = Column(Unicode(1000))
    type = Column(Integer) #1 -> imagen ##  0 -> video
    start = Column(DateTime)