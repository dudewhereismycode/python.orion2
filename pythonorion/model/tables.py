from sqlalchemy import Column, Integer, Date, Text, Unicode, Boolean, Numeric,Float,LargeBinary,ForeignKey
from sqlalchemy.types import DateTime
from datetime import datetime
from pythonorion.model import DeclarativeBase
from sqlalchemy import Table
from pythonorion.model import DeclarativeBase, metadata, DBSession
from sqlalchemy.orm import relation,backref,relationship
from sqlalchemy import Table, ForeignKey, Column
from sqlalchemy.types import Unicode, Integer, DateTime
from sqlalchemy.orm import relation, synonym

usuario_pronostico_table = Table('usuario_pronostico',metadata,
    Column('idusuario', Integer, ForeignKey('usuariosparticipantestable.idusuario')),
    Column('idpronostico', Integer, ForeignKey('pronosticotable.idpronostico'))
)


pronostico_partido_table = Table('pronostico_partido',metadata,
    Column('idpronostico', Integer, ForeignKey('pronosticotable.idpronostico')),
    Column('idpartido', Integer, ForeignKey('partidotable.idpartido'))
)

class UsuariosParticipantes(DeclarativeBase):
    __tablename__='usuariosparticipantestable'
    idusuario=Column(Integer,primary_key=True)
    username=Column(Unicode(200))
    nombre=Column(Unicode(200))
    telefono=Column(Unicode(50))
    lugarmexico=Column(Integer)
    puntos=Column(Integer)
    puntosoctavos =Column(Integer)
    puntoscuartos = Column(Integer)
    puntossemifinal = Column(Integer)
    puntostercerlugar = Column(Integer)
    puntosfinal = Column(Integer)
    ingresa=Column(Integer)
    posicion=Column(Integer)
    pronosticos=relationship("Pronostico",secondary=usuario_pronostico_table,back_populates="participantes")


class Pronostico(DeclarativeBase):
    __tablename__='pronosticotable'
    idpronostico=Column(Integer,primary_key=True)
    nombrepronostico=Column(Unicode(200))
    puntoslocal=Column(Integer)
    puntosvisita = Column(Integer)
    prediccionpartido=Column(Unicode(200))
    puntosconseguidos=Column(Integer)
    participantes = relationship("UsuariosParticipantes", secondary=usuario_pronostico_table, back_populates="pronosticos")
    partidos = relationship("Partido", secondary=pronostico_partido_table,back_populates="prono")


class Partido(DeclarativeBase):
    __tablename__='partidotable'
    idpartido=Column(Integer,primary_key=True)
    alias = Column(Unicode(200))
    nombrelocal=Column(Unicode(200))
    nombrevisita=Column(Unicode(200))
    puntoslocal=Column(Integer)
    puntosvisita = Column(Integer)
    fecha = Column(Unicode(200))
    estadopartido=Column(Unicode(200))
    fase=Column(Unicode(50))
    habilitado=Column(Integer)
    prono = relationship("Pronostico",secondary=pronostico_partido_table,back_populates="partidos")


class Finalposicion(DeclarativeBase):
    __tablename__='finalposicion'
    id_finalposicion=Column(Integer,primary_key=True)
    username=Column(Unicode(50))
    nombre=Column(Unicode(50))
    partido=Column(Unicode(50))
    puntostotales=Column(Integer)

class Kananregistro(DeclarativeBase):
    __tablename__='kananregistro'
    id_kananregistro=Column(Integer,primary_key=True)
    nombre=Column(Unicode(50))
    appaterno = Column(Unicode(50))
    apmaterno= Column(Unicode(50))
    email=Column(Unicode(100))
    celular = Column(Unicode(15))
    qr = Column(LargeBinary(length=(2 ** 32) - 1))

