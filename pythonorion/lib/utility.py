from pythonorion.lib.helpers import whoami
import os,sys
from tg import abort
from pathlib import Path
import pdfkit
from tg import render_template


class ExportPDF():
    @classmethod
    def create(cls, parameters,name,url,user):
        cwd=os.getenv('ORION_DIR')
        np=cwd.rfind("/")+1
        who=whoami()
        if user=="":
            error="CSV Failure"
            reason="User not logged"
            message = "The following {} occured, this is due to {}, please DEBUG the url : {}".format(error, reason,url)
            abort(status_code=500, detail=message)
        file_name=cwd+"/"+cwd[np:].replace(".","")+"/public/"+name+".pdf"


        my_file = Path(file_name)

        #print("my_file", my_file)
        if my_file.is_file():
            os.remove(file_name)

        ## PDF Generation ##
        options = {
            'page-size': 'Letter',
            'margin-top': '0.75in',
            'margin-right': '0.75in',
            'margin-bottom': '0.75in',
            'margin-left': '0.75in',
            'encoding': "UTF-8",
            'custom-header': [
                ('Accept-Encoding', 'gzip')
            ]
        }

        #print("URL: ",url)
        body = render_template(parameters, "mako", url)
        #print("body: ", body)
        pdfkit.from_string(body, file_name, options=options)
        #mifile=cwd+"/"+cwd[np:].replace(".","")+"/templates/BillingStatement/htmlpdf.mak"
        #pdfkit.from_file(mifile, file_name)



        return "/"+name+".pdf"
