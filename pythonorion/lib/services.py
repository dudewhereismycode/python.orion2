from tg import expose, flash
from tg.i18n import ugettext as _, lazy_ugettext as l_
from tg.predicates import has_permission
from tg import predicates, require

from pythonorion.lib.base import BaseController

__all__ = ['ServicesController']



class ServicesController(BaseController):
    """Sample controller-wide authorization"""


    @expose('json')
    def listjson(self, **kw):
        lista = []
        lista.append(
            {'id': 500, 'name': _('Sources'), 'rows': 1, 'cols': 4, 'url': '', 'observations': _('Orion Sources'),'perm': 'Orion Sources', 'open': 'Menu',
             'sons': [
                {'id': 501, 'name': _('Upload Promo'), 'url': '/promos/uploadpromo','observations': _('Promo Orion'), 'perm': 'Promos Orion', 'open': 'openWindow', 'sons': '','icon': '<span class="glyphicon glyphicon-cloud-upload"></span>'},
                 {'id': 502, 'name': _('Promo Platform'), 'url': '/promos/showpromo2', 'observations': _('Open Promo Orion'),'perm': 'Open Promo Orion', 'open': 'openTab', 'sons': '','icon': '<span class="glyphicon glyphicon-certificate"></span>'},
                 {'id': 507, 'name': _('Promo App'), 'url': '/promos/showpromo', 'observations': _('Open Promo Orion'),
                  'perm': 'Open Promo App Orion', 'open': 'openTab', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-certificate"></span>'},
                 {'id': 503, 'name': _('Links'), 'url': '/promos/showlinks', 'observations': _('Links Orion'),
                  'perm': 'Links Orion', 'open': 'openTab', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-link"></span>'},
                 {'id': 505, 'name': _('Upload Calendar'), 'url': '/manual/uploadpromo',
                  'observations': _('Upload Calendar Orion'),
                  'perm': 'Upload Calendar Orion', 'open': 'openWindow', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-cloud-upload"></span>'},
                 {'id': 504, 'name': _('Calendar'), 'url': '/promos/calendary', 'observations': _('Calendar Orion'),
                  'perm': 'Calendar Orion', 'open': 'openTab', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-calendar"></span>'},
                 {'id': 506, 'name': _('Images'), 'url': '/promos/images', 'observations': _('Images Orion'),
                  'perm': 'Images Orion', 'open': 'openTab', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-calendar"></span>'},
             ]})

        lista.append(
            {'id': 510, 'name': _('Log'), 'rows': 1, 'cols': 1, 'url': '', 'observations': _('Orion Log'),
             'perm': 'Orion Log', 'open': 'Menu',
             'sons': [
                 {'id': 511, 'name': _('Promos Views'), 'url': '/promos/promo_view', 'observations': _('Promo View Orion'),
                  'perm': 'Promo View  Orion', 'open': 'openTab', 'sons': '',
                  'icon': '<span class="glyphicon glyphicon-list-alt"></span>'},
             ]})
        return dict(planet_name='orion',list=lista,error='ok',id=60019)