# -*- coding: utf-8 -*-

"""The application's Globals object"""

__all__ = ['Globals']
import os
import urllib3

class Globals(object):
    """Container for objects available throughout the life of the application.

    One instance of Globals is created during application initialization and
    is available during requests via the 'app_globals' variable.

    Insert this at ~./bashrc

    export ORION_HOST=orion.dudewhereismy.com.mx
    export ORION_PORT=8085
    export ORION_SECURE=True
    export ORION_DIR=/home/wsgi/public_wsgi/python.orion


    """

    def __init__(self):
        """Do nothing, by default."""
        urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

        # Server parameters

        self.host_secure = os.getenv('ORION_SECURE')
        if self.host_secure is None:
            self.host_secure="False"

        if self.host_secure=="False":
            host_prefix="http://"
        else:
            host_prefix="https://"

        host_port=os.getenv('ORION_PORT')
        if host_port is None:
            host_port=""

        if len(host_port)>0:
            self.host_port=":"+host_port
        else:
            self.host_port=""

        self.host=os.getenv('ORION_HOST')
        if self.host is None:
            self.host=host_prefix+"localhost"
        else:
            self.host=host_prefix+self.host

        self.sun = os.getenv('SUN_HOST')
        if self.sun is None:
            self.sun = host_prefix + "localhost" + self.host_port
        else:
            self.sun = host_prefix + self.sun

        self.mercury = os.getenv('MERCURY_HOST')
        if self.mercury is None:
            self.mercury = host_prefix + "localhost" + self.host_port
        else:
            self.mercury = host_prefix + self.mercury

        self.venus = os.getenv('VENUS_HOST')
        if self.venus is None:
            self.venus = host_prefix + "localhost" + self.host_port
        else:
            self.venus = host_prefix + self.venus

        self.earth = os.getenv('EARTH_HOST')
        if self.earth is None:
            self.earth = host_prefix + "localhost" + self.host_port
        else:
            self.earth = host_prefix + self.earth

        self.mars = os.getenv('MARS_HOST')
        if self.mars is None:
            self.mars = host_prefix + "localhost" + self.host_port
        else:
            self.mars = host_prefix + self.mars

        self.jupiter = os.getenv('JUPITER_HOST')
        if self.jupiter is None:
            self.jupiter = host_prefix + "localhost" + self.host_port
        else:
            self.jupiter = host_prefix + self.jupiter

        self.pluto = os.getenv('PLUTO_HOST')
        if self.pluto is None:
            self.pluto = host_prefix + "localhost" + self.host_port
        else:
            self.pluto = host_prefix + self.pluto

        self.cygnus = os.getenv('CYGNUS_HOST')
        if self.cygnus is None:
            self.cygnus = host_prefix + "localhost" + self.host_port
        else:
            self.cygnus = host_prefix + self.cygnus

        self.apimars = os.getenv('API_MARS')
        if self.apimars is None:
            self.apimars = host_prefix + "localhost" + self.host_port
        else:
            self.apimars = host_prefix + self.apimars

        self.apimercury = os.getenv('API_MERCURY')
        if self.apimercury is None:
            self.apimercury = host_prefix + "localhost" + self.host_port
        else:
            self.apimercury = host_prefix + self.apimercury

        self.apisun = os.getenv('API_SUN')
        if self.apisun is None:
            self.apisun = host_prefix + "localhost" + self.host_port
        else:
            self.apisun = host_prefix + self.apisun

        self.apivenus = os.getenv('API_VENUS')
        if self.apivenus is None:
            self.apivenus = host_prefix + "localhost" + self.host_port
        else:
            self.apivenus = host_prefix + self.apivenus

        self.apicygnus = os.getenv('API_CYGNUS')
        if self.apicygnus is None:
            self.apicygnus = host_prefix + "localhost" + self.host_port
        else:
            self.apicygnus = host_prefix + self.apicygnus

        self.apijupiter = os.getenv('API_JUPITER')
        if self.apijupiter is None:
            self.apijupiter = host_prefix + "localhost" + self.host_port
        else:
            self.apijupiter = host_prefix + self.apijupiter

        self.app_dir = os.getenv('ORION_DIR')
        if self.app_dir is None:
            self.app_dir=os.getcwd()

        # STOMP parameters
        self.stomp_secure=os.getenv('STOMP_SECURE')
        if self.stomp_secure is None:
            self.stomp_secure="False"

        if self.stomp_secure=="False":
            stomp_prefix="ws://"
        else:
            stomp_prefix="wss://"

        stomp_port=os.getenv('STOMP_PORT')
        if stomp_port is None:
            stomp_port=""
        if len(stomp_port)>1:
            self.stomp_port=":"+stomp_port
        else:
            self.stomp_port=""

        self.stompHost = os.getenv('STOMP_HOST')
        if self.stompHost is None:
            self.stompHost = "localhost"

        self.stompUserName = os.getenv('STOMP_MASTER_USER')
        if self.stompUserName is None:
            self.stompUserName = "test"

        self.stompPassword = os.getenv('STOMP_MASTER_PASS')
        if self.stompPassword  is None:
            self.stompPassword = "test"

        self.stompServer=stomp_prefix+self.stompHost+':15671'+'/ws'

