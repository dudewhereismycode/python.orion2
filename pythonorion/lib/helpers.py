# -*- coding: utf-8 -*-
"""Template Helpers used in python.orion."""
import logging
from markupsafe import Markup
from datetime import datetime
from tg import request
from tg import app_globals

log = logging.getLogger(__name__)


def current_year():
    now = datetime.now()
    return now.strftime('%Y')


def icon(icon_name):
    return Markup('<i class="glyphicon glyphicon-%s"></i>' % icon_name)


# Import commonly used helpers from WebHelpers2 and TG
from tg.util.html import script_json_encode

try:
    from webhelpers2 import date, html, number, misc, text
except SyntaxError:
    log.error("WebHelpers2 helpers not available with this Python Version")

def url():
    return "https://orion.dudewhereismy.mx/"
    #return "https://orion.dudewhereismy.mx"

def urlpluto():
    return "https://pluto.dudewhereismy.com.mx/"
    #return "https://pluto.dudewhereismy.mx"

def urlsun():
    return "https://sun.dudewhereismy.com.mx/"
    #return "https://sun.dudewhereismy.mx"


def urlapi4sun():
    return "https://api4sun.dudewhereismy.com.mx/"

def whoami():
    try:
        ret=request.identity["repoze.who.userid"]
    except:
        return ""
    else:
        return ret